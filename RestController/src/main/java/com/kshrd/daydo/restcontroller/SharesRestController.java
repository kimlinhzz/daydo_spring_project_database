package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.SharedTask;
import com.kshrd.daydo.model.SimplySubTask;
import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.service.ShareTaskService.SharesService;
import com.kshrd.daydo.service.SimplyTaskService.SimplyTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class SharesRestController {

    @Autowired
    SharesService sharesService;
    @Autowired
    SimplyTaskService simplyTaskService;

    @PostMapping("/v1/api/shares/tasks")
    ResponseEntity<Map<String, Object>> insertShareTask(@RequestBody SharedTask sharedTask) {

        Map<String, Object> response = new HashMap<>();
        if (sharesService.updateSimplyTaskIsShared(sharedTask.getTaskId())) {
            if (sharesService.existedShare(sharedTask) == 1) {
                if (sharesService.insertTaskToExistedUser(sharedTask)) {
                    response.put("message", "DATA SUCCESSFULLY INSERTED.");
                    response.put("status", true);
                    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
                } else {
                    response.put("message", "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
                    response.put("status", true);
                    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
                }
            } else {

                if (sharesService.insertShareTask(sharedTask)) {
                    response.put("message", "DATA SUCCESSFULLY INSERTED.");
                    response.put("status", true);
                    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
                } else {
                    response.put("message", "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
                    response.put("status", false);
                    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
                }
            }
        }else {
            response.put("message", "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status", false);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * 'Update Share Task'
     * @param simplyTask
     * @return
     */
    @PutMapping("/v1/api/share/tasks")
    public ResponseEntity<Map<String , Object>> updateShareTasksById(@RequestBody SimplyTask simplyTask){
        Map<String , Object> response = new HashMap<>();
        if (simplyTaskService.updateSimplyTasks(simplyTask)){
            response.put("data" , simplyTaskService.getSimplyTaskById(simplyTask.getId()));
            response.put("message","DATA SUCCESSFULLY UPDATED.");
            response.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.OK);
        }else {
            response.put("data" , null);
            response.put("message","THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }

    /**
     *
     * @param 'Need parent ID of Task'
     * @return
     */
    @PostMapping("/v1/api/share/subtasks")
    public ResponseEntity<Map<String , Object> > addShareSubTasks(@RequestBody SimplySubTask simplySubTask){
        Map<String , Object> response = new HashMap<>();
        if (simplyTaskService.insertSimplySubTask(simplySubTask)){
            response.put("data" , simplyTaskService.getLastestSimplySubTasksByUserId(simplySubTask.getParentId(),simplySubTask.getuID()));
            response.put("message","DATA SUCCESSFULLY INSERTED");
            response.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.CREATED);
        }else {
            response.put("data" ,null);
            response.put("message","THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }


    @PutMapping("/v1/api/share/subtasks")
    public ResponseEntity<Map<String , Object> > updateSimplySubTasks(@RequestBody SimplySubTask simplySubTask){
        Map<String , Object> respose = new HashMap<>();
        if (simplyTaskService.updateSimplySubTasks(simplySubTask)){
            respose.put("data" , simplyTaskService.getSimplyTaskById(simplySubTask.getId()));
            respose.put("message","DATA SUCCESSFULLY UPDATED.");
            respose.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.OK);
        }else {
            respose.put("message","THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            respose.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/v1/api/share/subtasks/{subID}")
    public ResponseEntity<Map<String , Object> > deleteShareSubTask(@RequestParam("subID") int id){
        Map<String , Object> respose = new HashMap<>();
        if (simplyTaskService.deleteSimplySubTaskById(id)){
            respose.put("message","DATA SUCCESSFULLY DELETED.");
            respose.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.OK);
        }else {
            respose.put("message","THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            respose.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }



    @PutMapping("/v1/api/shares/tasks/user/role")
    ResponseEntity<Map<String, Object>> updateShareRoleByUserIdAndTaskId(@RequestParam("userID") int userId, @RequestParam("taskID") int taskId, @RequestParam("roleID") int roleId){
        Map<String , Object> response = new HashMap<>();
        if (sharesService.updateShareRoleByUserIdAndTaskId(userId, taskId, roleId)){
            response.put("message" , "DATA SUCCESSFULLY UPDATED.");
            response.put("status" , true);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("message" , "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }



    @PutMapping("/v1/api/shares/tasks/")
    ResponseEntity<Map<String, Object>> deleteShareByShareId(@RequestParam("userID") int userId, @RequestParam("taskID") int taskId){
        Map<String , Object> response = new HashMap<>();
        if (sharesService.deleteShareTask(userId, taskId)){
            response.put("message" , "DATA SUCCESSFULLY DELETED.");
            response.put("status" , true);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }else {
            response.put("message" , "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }
    }

    @GetMapping("/v1/api/shares/task/user")
    ResponseEntity<Map<String, Object>> getAllSharedTaskByUserId(@RequestParam("userID") int userId){
        List<SharedTask> shareTask = sharesService.getAllSharedTaskByUserId(userId);
        Map<String , Object> response = new HashMap<>();
        if (shareTask.isEmpty()){
            response.put("data" ,null);
            response.put("message" , "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("data",shareTask);
            response.put("message" , "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status" , true);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }
}
