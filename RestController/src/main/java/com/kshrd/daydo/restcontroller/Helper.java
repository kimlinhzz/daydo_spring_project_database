package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.Notitification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public class Helper {
    public static ResponseEntity<Map<String, Object>> getAll(Map response, List objects) {
        if (objects.isEmpty()) {
            response.put("message", "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status", false);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        } else {
            response.put("data", objects);
            response.put("message", "DATA IS AVAILABLE RIGHT NOW");
            response.put("status", true);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }

    public static ResponseEntity<Map<String, Object>> insert(Map response, List objects) {

        return null;
    }
}
