package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.Progress;
import com.kshrd.daydo.model.RoutineSubTask;
import com.kshrd.daydo.model.RoutineTask;
import com.kshrd.daydo.service.RoutineTaskService.RoutineTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RoutineTaskRestController  {

    @Autowired
    RoutineTaskService routineTaskService;

    @PostMapping("v1/api/routine/tasks")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Map<String , Object>> insertRoutineTask(@RequestBody RoutineTask routineTask){
        Map<String , Object> response = new HashMap<>();
        if (routineTaskService.insertRoutineTask(routineTask)){
            response.put("message", "DATA SUCCESSFULLY INSERTED.");
            response.put("status", true);
            response.put("data", routineTaskService.getLastestRoutineTaskById(routineTask.getUserId()));
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.OK);
        }else {
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data" , null);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("v1/api/routine/subtasks")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Map<String , Object>> insertRoutineSubTask(@RequestBody RoutineSubTask routineSubTask){
        Map<String , Object> response = new HashMap<>();
        if (routineTaskService.insertRoutineSubTask(routineSubTask)){
            response.put("message", "DATA SUCCESSFULLY INSERTED");
            response.put("status", true);
            response.put("data", routineTaskService.getLastestRoutineSubTaskById(routineSubTask.getParentId(), routineSubTask.getUserId()));
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.OK);
        }else {
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);

            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("v1/api/routine/tasks/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Map<String , Object>> deleteRoutineTask(@PathVariable("id") int taskId){
        Map<String , Object> response = new HashMap<>();
        if (routineTaskService.deleteRoutineTask(taskId)){
            response.put("message", "DATA SUCCESSFULLY DELETED");
            response.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.OK);
        }else {
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("v1/api/routine/subtasks/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Map<String , Object>> deleteRoutineSubTask(@PathVariable("id") int taskId){
        Map<String , Object> respose = new HashMap<>();
        if (routineTaskService.deleteRoutineSubTask(taskId)){
            respose.put("message", "DATA SUCCESSFULLY DELETED");
            respose.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.OK);
        }else {
            respose.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            respose.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("v1/api/routine/tasks")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Map<String , Object>> updateRoutineTask(@RequestBody RoutineTask routineTask){
        Map<String , Object> respose = new HashMap<>();
        if (routineTaskService.updateRoutineTask(routineTask)){
            respose.put("message", "DATA SUCCESSFULLY UPDATED");
            respose.put("status" , true);
            respose.put("data" , routineTaskService.getRoutineTaskById(routineTask.getId()));
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.OK);
        }else {
            respose.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            respose.put("status" , false);
            respose.put("data" , null);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("v1/api/routine/tasks")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Map<String , Object>> findRoutineTaskByID(@RequestParam("routineID") int routineID){
        Map<String , Object> respose = new HashMap<>();
        if (routineTaskService.getRoutineTaskById(routineID)!=null){
            respose.put("message", "DATA SUCCESSFULLY UPDATED");
            respose.put("status" , true);
            respose.put("data" , routineTaskService.getRoutineTaskById(routineID));
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.OK);
        }else {
            respose.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            respose.put("status" , false);
            respose.put("data" , null);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }


    @PutMapping("v1/api/routine/subtasks")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Map<String , Object>> updateRoutineSubTask(@RequestBody RoutineSubTask routineSubTask){
        Map<String , Object> respose = new HashMap<>();
        if (routineTaskService.updateRoutineSubTask(routineSubTask)){
            respose.put("message", "DATA SUCCESSFULLY UPDATED");
            respose.put("status" , true);
            respose.put("data" , routineTaskService.getRoutineSubTaskById(routineSubTask.getId()));
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.OK);
        }else {
            respose.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            respose.put("status" , false);
            respose.put("data" , null);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }


    @PutMapping("/v1/api/routine/subtasks/progress")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Map<String , Object>> updateRoutineSubTaskProgress(@RequestParam("id") int progressId
                                                ,@RequestParam("percentage") double progressPercentage){

        Map<String , Object> respose = new HashMap<>();
        if (routineTaskService.updateRoutineSubTaskProgress(progressId, progressPercentage)){
            respose.put("message","DATA SUCCESSFULLY UPDATE ");
            respose.put("status" , true);
            respose.put("data" , routineTaskService.getProgressById(progressId));
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.OK);
        }else {
            respose.put("message","REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            respose.put("status" , false);
            respose.put("data",null);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v1/api/routine/tasks/{userId}")
    public ResponseEntity<Map<String , Object>> getAllRoutineTasksByUserId(@PathVariable("userId") int userId){
        List<RoutineTask> routineTasks = routineTaskService.getAllRoutineTasksByUserId(userId);
        return apiResponse(routineTasks);
    }

    @GetMapping("/v1/api/routine/subtasks/progress")
    public Progress getProgressByTaskId(@RequestParam(name = "subTaskId") int taskID){
        return routineTaskService.getProgressByTaskId(taskID);
    }

    @GetMapping("/v1/api/routine/subtasks/{parentID}")
    public ResponseEntity<Map<String , Object>> getRoutineSubTasksByParentId(@PathVariable("parentID") int parentID){
        List<RoutineSubTask> routineSubTasks = routineTaskService.getRoutineSubTasksByParentId(parentID);
        return apiResponse(routineSubTasks);
    }

    // Todo Api Response with Generic Type
    private ResponseEntity<Map<String , Object>> apiResponse(List<?> tasks){
        Map<String , Object> response = new HashMap<>();
        if (tasks.isEmpty()){
            response.put("message" , "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data" , null);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("message" , "DATA IS AVAILABLE RIGHT NOW");
            response.put("status" , true);
            response.put("data", tasks);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
//        return response;
    }
}
