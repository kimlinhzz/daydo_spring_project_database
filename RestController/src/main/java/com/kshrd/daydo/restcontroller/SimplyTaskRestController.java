package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.SimplySubTask;
import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.service.SimplyTaskService.SimplyTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class SimplyTaskRestController {

    @Autowired
    SimplyTaskService simplyTaskService;

    @PutMapping("/v1/api/simply/tasks")
       public ResponseEntity<Map<String , Object> > updateSimplyTasks(@RequestBody SimplyTask simplyTask){
            Map<String , Object> response = new HashMap<>();
            if (simplyTaskService.updateSimplyTasks(simplyTask)){
                response.put("data" , simplyTaskService.getSimplyTaskById(simplyTask.getId()));
                response.put("message","DATA SUCCESSFULLY UPDATED.");
                response.put("status" , true);
                return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.OK);
            }else {
                response.put("message","REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
                response.put("status" , false);
                return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
            }
    }

    @PutMapping("/v1/api/simply/subtasks")
    public ResponseEntity<Map<String , Object> > updateSimplySubTasks(@RequestBody SimplySubTask simplySubTask){
        Map<String , Object> respose = new HashMap<>();
        if (simplyTaskService.updateSimplySubTasks(simplySubTask)){
            respose.put("data" , simplyTaskService.getSimplyTaskById(simplySubTask.getId()));
            respose.put("message","Update Sub SimplyTask Successfully");
            respose.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.OK);
        }else {
            respose.put("message","Update Sub SimplyTask Fail");
            respose.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }
    @PutMapping("/v1/api/simply/tasks/{id}")
    public ResponseEntity<Map<String , Object> > deleteSimplyTask(@PathVariable("id") int id){
        Map<String , Object> response = new HashMap<>();
        if (simplyTaskService.deleteSimplyTaskById(id)){
            response.put("message","DATA SUCCESSFULLY DELETE.");
            response.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.OK);
        }else {
            response.put("message","REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/v1/api/simply/subtasks/{id}")
    public ResponseEntity<Map<String , Object> > deleteSimplySubTask(@PathVariable("id") int id){
        Map<String , Object> response = new HashMap<>();
        if (simplyTaskService.deleteSimplySubTaskById(id)){
            response.put("message","DATA SUCCESSFULLY DELETED.");
            response.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.OK);
        }else {
            response.put("message","REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/v1/api/simply/tasks")
    public ResponseEntity<Map<String , Object> > addSimplyTasks(@RequestBody SimplyTask simplyTask){
        Map<String , Object> respose = new HashMap<>();
        if (simplyTaskService.insertSimplyTask(simplyTask)){
            respose.put("message","DATA SUCCESSFULLY INSERTED.");
            respose.put("status" , true);
            respose.put("data" , simplyTaskService.getLastestSimplyTasksByUserId(simplyTask.getuID()));
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.CREATED);
        }else {
            respose.put("message","REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            respose.put("status" , false);
            respose.put("data" , null);
            return  new ResponseEntity<Map<String , Object>> (respose , HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/v1/api/simply/subtasks")
    public ResponseEntity<Map<String , Object> > addSimplySubTasks(@RequestBody SimplySubTask simplySubTask){
        Map<String , Object> response = new HashMap<>();
        if (simplyTaskService.insertSimplySubTask(simplySubTask)){

            response.put("message","DATA SUCCESSFULLY INSERTED.");
            response.put("status" , true);
            response.put("data" , simplyTaskService.getLastestSimplySubTasksByUserId(simplySubTask.getParentId(),simplySubTask.getuID()));
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.CREATED);
        }else {
            response.put("message","REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data" , null);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v1/api/simply/tasks/user")
    public ResponseEntity<Map<String , Object>> getSimplyTaskByUserIdAndDate(@RequestParam("userID") int userID , @RequestParam("type") String type )
    {
        List<SimplyTask> simplyTasks = simplyTaskService.getSimplyTasksByUserIdAndDate(userID , type);
        return apiResponse(simplyTasks);
    }

    @GetMapping("/v1/api/simply/tasks/{id}")
    public ResponseEntity<Map<String , Object>> getSimplyTaskById(@PathVariable("id") int taskID )
    {
        SimplyTask  simplyTask = simplyTaskService.getSimplyTaskById(taskID);
        Map<String , Object> response = new HashMap<>();
        if (simplyTask==null){
            response.put("message" , "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data",null);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("message" , "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status" , true);
            response.put("data",simplyTask);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }
    // Todo Api Response with Generic Type
    private ResponseEntity<Map<String , Object>> apiResponse(List<?> tasks){
        Map<String , Object> response = new HashMap<>();
        if (tasks.isEmpty()){
            response.put("message" , "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data" , null);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("message" , "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status" , true);
            response.put("data", tasks);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }
}
