package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.service.SimplyTaskService.SimplyTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ArchiveController {
    @Autowired
    SimplyTaskService simplyTaskService;

    @GetMapping("/v1/api/archives/user")
    public ResponseEntity<Map<String, Object>> getAllArchivesByUserID(@RequestParam("userID") int userID) {
        List<SimplyTask> simplyTasks = simplyTaskService.getAllArchiveByUserId(userID);
        return apiResponse(simplyTasks);
    }

    @PutMapping("/v1/api/archive/{task_id}")
    public ResponseEntity<Map<String, Object>> updateSimplyToArchive(@PathVariable("task_id") int taskID ,@RequestParam("is_archive") boolean isArchive) {
        Map<String, Object> response = new HashMap<>();
        if (simplyTaskService.updateToArchive(taskID , isArchive)) {
            response.put("data", simplyTaskService.getSimplyTaskById(taskID));
            response.put("message", "DATA SUCCESSFULLY UPDATED.");
            response.put("status", true);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
        } else {
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status", false);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
    }
    @PutMapping("/v1/api/remove/archive/{task_id}")
    public ResponseEntity<Map<String , Object> > deleteArchive(@PathVariable("task_id") int id){
        Map<String , Object> response = new HashMap<>();
        if (simplyTaskService.deleteArchiveById(id)){
            response.put("message","DATA SUCCESSFULLY DELETED.");
            response.put("status" , true);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.OK);
        }else {
            response.put("message","REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<Map<String , Object>> (response , HttpStatus.NOT_FOUND);
        }
    }

    // Todo Api Response with Generic Type
    private ResponseEntity<Map<String, Object>> apiResponse(List<?> tasks) {
        Map<String, Object> response = new HashMap<>();
        if (tasks.isEmpty()) {
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status", false);
            response.put("data", null);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        } else {
            response.put("message", "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status", true);
            response.put("data", tasks);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
        }
    }
}
