package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.Motivation;
import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.service.MotivationService.MotivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MotivationRestController {

    @Autowired
    MotivationService motivationService;


    @PostMapping("v1/api/motivations")
    public ResponseEntity<Map<String , Object>> insertMotivation(@RequestBody Motivation motivation){

        Map<String , Object> response = new HashMap<>();
        if (motivationService.insertMotivation(motivation)){
            response.put("message" , "DATA IS SUCCESSFULLY INSERTED");
            response.put("status" , true);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("message" , "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }
    @GetMapping("v1/api/motivations")
    public ResponseEntity<Map<String , Object>> getAllMotivations(){
        List<Motivation> motivations = motivationService.getAllMotivations();
        Map<String , Object> response = new HashMap<>();
        if (motivations.isEmpty()){
            response.put("message" , "THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("data",motivations);
            response.put("message" , "DATA IS AVAILABLE RIGHT NOW");
            response.put("status" , true);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }
}
