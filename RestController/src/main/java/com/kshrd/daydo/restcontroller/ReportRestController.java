package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.Report;
import com.kshrd.daydo.model.ReportModel.DailyReport;
import com.kshrd.daydo.service.ReportService.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ReportRestController {

    @Autowired
    ReportService reportService;

    @GetMapping("/v1/api/report/daily")
    ResponseEntity<Map<String, Object>> getDailyReport(@RequestParam("uID") int uID, @RequestParam("date") String onDate){
        Map<String, Object> response = new HashMap<>();
        Timestamp timestamp = Timestamp.valueOf(onDate + " 00:00:00.00");
        List<DailyReport> dailyReport = reportService.getDailyReportOnDate(uID, timestamp, timestamp);
        response.put("data", dailyReport);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }

    @GetMapping("/v1/api/report")
    ResponseEntity<Map<String, Object>> getDailyReport(@RequestParam("uID") int uID, @RequestParam("from") String fromDate, @RequestParam("to") String toDate){
        Map<String, Object> response = new HashMap<>();
        Timestamp fromTimestamp = Timestamp.valueOf(fromDate + " 00:00:00.00");
        Timestamp toTimestamp = Timestamp.valueOf(toDate + " 00:00:00.00");
        List<DailyReport> dailyReport = reportService.getDailyReportOnDate(uID, fromTimestamp, toTimestamp);
        if (dailyReport.isEmpty()){
            response.put("message" , "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status" , false);
           response.put("data" , null);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }else {
            response.put("message" , "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status" , true);
            response.put("data", dailyReport);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
        }

    }
}
