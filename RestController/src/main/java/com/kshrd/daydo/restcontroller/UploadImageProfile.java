package com.kshrd.daydo.restcontroller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/v1/api")
public class UploadImageProfile {

    @Value("${imagePath}")
    String imagePath;

        @PostMapping("/images")
        public ResponseEntity<Map<String, Object>> fileUpload(MultipartFile file) {
            String fileName ;
            Map<String, Object> response = new HashMap<>();
            try {
                fileName = uploaded(file);
            } catch (Exception ex) {
                ex.printStackTrace();
                response.put("status", false);
                response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
                return new ResponseEntity<>(response , HttpStatus.NOT_FOUND);
            }
            response.put("status", true);
            response.put("message", "FILE SUCCESSFULLY UPLOADED.");
            response.put("data",fileName);
            return new ResponseEntity<>(response , HttpStatus.OK);
        }

        public String uploaded(MultipartFile files) throws Exception {
            String fileName = UUID.randomUUID() + "." + Objects.requireNonNull(files.getOriginalFilename())
                    .substring(files.getOriginalFilename().lastIndexOf(".") + 1);
            Files.copy(files.getInputStream(), Paths.get(imagePath, fileName));
            return fileName;
        }
    }

