package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.Notitification;
import com.kshrd.daydo.service.NotificationService.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class NotificationRestController {

    NotificationService notificationService;

    @GetMapping("/v1/api/notifications")
    public ResponseEntity<Map<String, Object>> getAllNotifications() {
        Map<String, Object> response = new HashMap<>();
        List<Notitification> notifications = notificationService.getAllNotifications();
        return Helper.getAll(response, notifications);
    }

    @GetMapping("/v1/api/notifications/user")
    public ResponseEntity<Map<String, Object>> getNotificationByUserId(@RequestParam("userId") Integer userId) {
        Map<String, Object> response = new HashMap<>();
        List<Notitification> notifications = notificationService.getNotificationByUserId(userId);
        return Helper.getAll(response, notifications);
    }

    @PostMapping("/v1/api/notifications")
    public ResponseEntity<Map<String, Object>> insertNotification(@RequestBody Notitification notification) {
        Map<String, Object> response = new HashMap<>();
        if (notificationService.insertNotification(notification)){
            response.put("data" , notificationService.getNotificationById(notification.getId()));
            response.put("message","DATA SUCCESSFULLY INSERTED.");
            response.put("status" , true);
            return  new ResponseEntity<> (response, HttpStatus.CREATED);
        }else {
            response.put("message","THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<> (response, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/v1/api/notifications/{id}")
    public ResponseEntity<Map<String, Object>> updateIsRead(@PathVariable Integer id) {
        Map<String, Object> response = new HashMap<>();
        if (notificationService.updateIsRead(id)){
            response.put("data" , notificationService.getNotificationById(id));
            response.put("message","DATA SUCCESSFULLY UPDATED.");
            response.put("status" , true);
            return  new ResponseEntity<> (response, HttpStatus.CREATED);
        }else {
            response.put("message","THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<> (response, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/v1/api/notification/{id}")
    public ResponseEntity<Map<String, Object>> updateIsDeleted(@PathVariable Integer id) {
        Map<String, Object> response = new HashMap<>();
        if (notificationService.updateIsDeleted(id)){
            response.put("data" , notificationService.getNotificationById(id));
            response.put("message","DATA SUCCESSFULLY DELETED.");
            response.put("status" , true);
            return  new ResponseEntity<> (response, HttpStatus.CREATED);
        }else {
            response.put("message","THE REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            return  new ResponseEntity<> (response, HttpStatus.NOT_FOUND);
        }
    }

    @Autowired
    public NotificationRestController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
}
