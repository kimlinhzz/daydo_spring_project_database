package com.kshrd.daydo.restcontroller;

import com.kshrd.daydo.model.User;
import com.kshrd.daydo.service.UserService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserRestController {

    @Autowired
    UserService userService;

    //TODO Login And SignIn User
    @PostMapping("/v1/api/user/sign/in")
    public ResponseEntity<Map<String , Object>> loginUser(@RequestBody User user){

        Map<String , Object> response = new HashMap<>();
        //Todo Login User If They Already Exit
        if (userService.loginUser(user.getAccessToken())!=null){
            user = userService.loginUser(user.getAccessToken());
            response.put("message", "USER SUCCESSFULLY LOGIN.");
            response.put("status" , true);
            response.put("data", user);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }else {
            //Todo Sign-In User If They're New
            if (userService.addUser(user)) {
            response.put("message", "USER SUCCESSFULLY SIGN-IN.");
            response.put("status" , true);
            response.put("data",userService.loginUser(user.getAccessToken()));
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.CREATED) ;
        }
       else {
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data", null);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }
        }

    }
    @GetMapping("/v1/api/users")
    public ResponseEntity<Map<String , Object> > getAllUsers(){
        List<User> users = userService.getAllUser();
        Map<String , Object> response = new HashMap<>();
        if (users.isEmpty()){
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data", null);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("message", "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status", true);
            response.put("data", users);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }

    @PutMapping("/v1/api/users")
    public ResponseEntity<Map<String, Object>> updateUser(@RequestBody User user) {
        Map<String, Object> response = new HashMap<>();
        if (userService.updateUser(user)) {
            response.put("message", "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status", true);
            response.put("data", getUserById(user.getId()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
        } else {
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status", false);
            response.put("data", null);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
    }



    @GetMapping("/v1/api/users/id/{userID}")
    public ResponseEntity<Map<String , Object> > getUserById(@PathVariable("userID") int userId){
       User user = userService.getUserById(userId);
        Map<String , Object> response = new HashMap<>();
        if (user == null){
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data", null);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }else {
            response.put("message", "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status" , true);
            response.put("data", user);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }

    @GetMapping("/v1/api/users/sender/id/{senderID}")
    public ResponseEntity<Map<String , Object> > findUserBySenderID(@PathVariable("senderID") String senderID){
        User user = userService.findUserBySenderId(senderID);
        Map<String , Object> response = new HashMap<>();
        if (user == null){
            response.put("message", "REQUESTED OPERATION FAILED BECAUSE A RESOURCE ASSOCIATED WITH THE REQUEST COULD NOT BE FOUND.");
            response.put("status" , false);
            response.put("data", null);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.NOT_FOUND) ;
        }else {
            response.put("message", "DATA IS AVAILABLE RIGHT NOW.");
            response.put("status", true);
            response.put("data", user);
            return new ResponseEntity<Map<String , Object>>(response, HttpStatus.OK) ;
        }
    }


}
