package com.kshrd.daydo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.kshrd")
public class DayDoUI extends SpringBootServletInitializer {


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(DayDoUI.class);
    }

    public static void main(String[] args) {
        try {
            SpringApplication.run(DayDoUI.class);
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }
}
