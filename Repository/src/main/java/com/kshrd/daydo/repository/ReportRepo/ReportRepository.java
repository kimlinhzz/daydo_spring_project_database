package com.kshrd.daydo.repository.ReportRepo;

import com.kshrd.daydo.model.Progress;
import com.kshrd.daydo.model.Report;
import com.kshrd.daydo.model.ReportModel.DailyReport;
import com.kshrd.daydo.model.ReportModel.ProgressReport;
import com.kshrd.daydo.model.ReportModel.RoutineReport;
import com.kshrd.daydo.model.ReportModel.SubRoutineReport;
import com.kshrd.daydo.model.RoutineSubTask;
import com.kshrd.daydo.model.RoutineTask;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface ReportRepository {
    @Select("SELECT progress_date::DATE, sum(progress_percentage) AS total_percentage, count(DISTINCT parent_task_id) AS total_tasks, count(DISTINCT task_id) AS total_sub_tasks " +
            "from v_report_1 " +
            "WHERE user_id = #{userId} AND progress_date::DATE BETWEEN #{fromDate}::DATE AND #{toDate}::DATE " +
            "GROUP BY progress_date ::DATE")
    @Results({
            @Result(property = "progressDate", column = "progress_date"),
            @Result(property = "totalTask", column = "total_tasks"),
            @Result(property = "totalTaskProgressPercentage", column = "total_percentage"),
            @Result(property = "totalSubTask", column = "total_sub_tasks")
    })
    List<DailyReport> getDailyReportOnDate(int userId, Timestamp fromDate, Timestamp toDate);

    @Select("SELECT * FROM dd_routine_tasks" +
            " WHERE user_id = #{userId} AND is_deleted = FALSE AND parent_task_id IS NULL")
    @Results({
            @Result(property = "taskId", column = "task_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "totalSubTask", column = "task_id", one = @One(select = "getTotalSubTask")),
            @Result(property = "subRoutineReportList", column = "task_id", many = @Many(select = "getAllSubRoutineReport"))
    })
    List<RoutineReport> getAllRoutineReport(int userId);

    @Select("SELECT * FROM dd_routine_tasks" +
            " WHERE parent_task_id = #{parentId} AND is_deleted = FALSE")
    @Results({
            @Result(property = "subTaskId", column = "task_id"),
            @Result(property = "parentTaskId", column = "parent_task_id"),
            @Result(property = "title", column = "title")
    })
    List<SubRoutineReport> getAllSubRoutineReport(int parentId);

    @Select("SELECT * FROM dd_progress" +
            " WHERE task_id = #{taskId} AND progress_date::DATE = #{onDate}::DATE")
    @Results({
            @Result(property = "progressId", column = "progress_id"),
            @Result(property = "progressPercentage", column = "progress_percentage")
    })
    ProgressReport getProgressReportOnDate(int taskId, Timestamp onDate);

    @Select("SELECT COUNT(parent_task_id) FROM dd_routine_tasks " +
            "WHERE parent_task_id = #{parentId} AND is_deleted = FALSE GROUP BY parent_task_id ")
    Integer getTotalSubTask(int parentId);

    @Select("SELECT sum(progress_percentage) from v_report_1 " +
            "WHERE parent_task_id = #{parentId} AND progress_date::DATE = #{onDate}::DATE " +
            "GROUP BY progress_date :: DATE")
    Double getTotalProgressOnEachTask(int parentId, Timestamp onDate);

}
