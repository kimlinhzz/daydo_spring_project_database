package com.kshrd.daydo.repository.RoutineTaskRepo;

import com.kshrd.daydo.model.Progress;
import com.kshrd.daydo.model.RoutineSubTask;
import com.kshrd.daydo.model.RoutineTask;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RoutineTaskRepository {

    @Select("SELECT * FROM dd_progress WHERE progress_id = #{progressID}")
    @Results({
            @Result(property = "id", column = "progress_id"),
            @Result(property = "taskId", column = "task_id"),
            @Result(property = "progressDate", column = "progress_date"),
            @Result(property = "progressPercentage", column = "progress_percentage"),
            @Result(property = "isDeleted", column = "is_deleted"),
            @Result(property = "createdDate", column = "created_date")
    })
    Progress getProgressById(int progressID);

    @Select("SELECT * FROM dd_routine_tasks WHERE task_id = #{taskID}")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "remindDate", column = "remind_date"),
            @Result(property = "isImportant", column = "is_important"),
            @Result(property = "isDeleted", column = "is_deleted"),
            @Result(property = "routineSubTasks", column = "task_id", many = @Many(select = "getRoutineSubTasksByParentId")),
    })
    RoutineTask getRoutineTaskById(int taskID);

    @Select("SELECT * FROM dd_routine_tasks WHERE user_id = #{userID} AND parent_task_id ISNULL  AND is_deleted = false " +
            " ORDER BY created_date desc " +
            " limit 1 ")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "remindDate", column = "remind_date"),
            @Result(property = "isImportant", column = "is_important"),
            @Result(property = "isDeleted", column = "is_deleted")
    })
    RoutineTask getLastestRoutineTaskById(int userID);

    @Select("SELECT * FROM dd_routine_tasks WHERE task_id = #{taskID}")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "parentId", column = "parent_task_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "isDeleted", column = "is_deleted"),
            @Result(property = "remindDate", column = "remind_date"),
            @Result(property = "isImportant", column = "is_important")
    })
    RoutineSubTask getRoutineSubTaskById(int taskID);


    @Select("SELECT * FROM dd_routine_tasks WHERE task_id = #{taskID} AND user_id = #{userID}  AND is_deleted = false " +
            " ORDER BY created_date desc" +
            " limit 1")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "parentId", column = "parent_task_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "isDeleted", column = "is_deleted"),
            @Result(property = "remindDate", column = "remind_date"),
            @Result(property = "isImportant", column = "is_important")
    })
    RoutineSubTask getLastestRoutineSubTaskById(int taskID , int userID);

    @Insert("INSERT INTO dd_routine_tasks(user_id, parent_task_id, title, note , remind_date, is_important, is_deleted)\n" +
            " VALUES (#{userId}, NULL, #{title}, #{note}, #{remindDate}, #{isImportant}, #{isDeleted});")
    boolean insertRoutineTask(RoutineTask routineTask);

    @Insert("INSERT INTO dd_routine_tasks(user_id, parent_task_id, title, note , remind_date, is_important, is_deleted)\n" +
            " VALUES (#{userId}, #{parentId}, #{title}, #{note} , #{remindDate}, #{isImportant}, #{isDeleted});")
    boolean insertRoutineSubTask(RoutineSubTask routineSubTask);

    @Update("UPDATE dd_routine_tasks set is_deleted = true where task_id = #{taskId} AND parent_task_id ISNULL AND is_deleted = false")
    boolean deleteRoutineTask(int taskId);

    @Update("UPDATE dd_routine_tasks set is_deleted = true where task_id = #{taskId} AND parent_task_id NOTNULL AND is_deleted = false")
    boolean deleteRoutineSubTask(int taskId);

    @Update("UPDATE dd_routine_tasks SET title = #{title}, note = #{note}, remind_date = #{remindDate}, " +
            "is_important = #{isImportant}, is_deleted = #{isDeleted} WHERE task_id = #{id} AND parent_task_id ISNULL;")
    boolean updateRoutineTask(RoutineTask routineTask);

    @Update("UPDATE dd_routine_tasks SET title = #{title}, note = #{note}, remind_date = #{remindDate}, " +
            "is_important = #{isImportant}, is_deleted = #{isDeleted} WHERE task_id = #{id} AND parent_task_id NOTNULL;")
    boolean updateRoutineSubTask(RoutineSubTask routineSubTask);

    @Update("UPDATE dd_progress SET progress_percentage = #{progressPercentage} WHERE progress_id = #{progressId};")
    boolean updateRoutineSubTaskProgress(Integer progressId, double progressPercentage);

    // Todo get all routine task
    @Select("SELECT * FROM dd_routine_tasks " +
            "WHERE user_id = #{userID} AND parent_task_id ISNULL AND is_deleted = FALSE " +
            "ORDER BY created_date desc")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "routineSubTasks", column = "task_id", many = @Many(select = "getRoutineSubTasksByParentId")),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "remindDate", column = "remind_date"),
            @Result(property = "isImportant", column = "is_important"),
            @Result(property = "isDeleted", column = "is_deleted")
    })
    List<RoutineTask> getAllRoutineTasksByUserId(int userID);

    // Todo get progress by task id
    @Select("SELECT * FROM dd_progress WHERE task_id = #{taskID} AND \n" +
            "progress_date::DATE = now()::DATE")
    @Results({
            @Result(property = "id", column = "progress_id"),
            @Result(property = "taskId", column = "task_id"),
            @Result(property = "progressDate", column = "progress_date"),
            @Result(property = "progressPercentage", column = "progress_percentage"),
            @Result(property = "isDeleted", column = "is_deleted"),
            @Result(property = "createdDate", column = "created_date")
    })
    Progress getProgressByTaskId(int taskID);

    @Insert("INSERT INTO dd_progress (task_id) Values (#{taskID})")
    boolean insertProgressForNewDay(int taskID);
    // get sub routine tasks by task ID
    @Select("SELECT * FROM dd_routine_tasks WHERE parent_task_id = #{parentTaskID} AND is_deleted = FALSE")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "parentId", column = "parent_task_id"),
            @Result(property = "title", column = "title"),
           @Result(property = "progress", column = "task_id", many = @Many(select = "getProgressByTaskId")),
            @Result(property = "note", column = "note"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "isDeleted", column = "is_deleted"),
            @Result(property = "remindDate", column = "remind_date"),
            @Result(property = "isImportant", column = "is_important")
    })
    List<RoutineSubTask> getRoutineSubTasksByParentId(int parentTaskID);
}
