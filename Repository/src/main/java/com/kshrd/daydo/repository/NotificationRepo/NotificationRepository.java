package com.kshrd.daydo.repository.NotificationRepo;

import com.kshrd.daydo.model.Notitification;
import com.kshrd.daydo.model.Role;
import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository {

    @InsertProvider(method = "insertNotification", type = NotificationProvider.class)
    boolean insertNotification(Notitification notification);

    @SelectProvider(method = "getAllNotifications", type = NotificationProvider.class)
    @Results({
            @Result(property = "id", column = "notification_id"),
            @Result(property = "userId", column = "receiver_id"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "isRead", column = "is_read"),
            @Result(property = "isDeleted", column = "is_deleted"),
    })
    List<Notitification> getAllNotifications();

    @SelectProvider(method = "getNotificationByUserId", type = NotificationProvider.class)
    @Results({
            @Result(property = "id", column = "notification_id"),
            @Result(property = "userId", column = "receiver_id"),
            @Result(property = "senderId" , column = "sender_id"),
            @Result(property = "taskId" , column = "task_id"),
            @Result(property = "roleId" , column = "role_id"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "isRead", column = "is_read"),
            @Result(property = "user", column = "sender_id" , one = @One(select = "getUserBySenderId")),
            @Result(property = "role", column = "role_id" , one = @One(select = "getRoleById")),
            @Result(property = "simplyTask", column = "task_id" , one = @One(select = "getTaskByShareTaskId")),
            @Result(property = "type" , column = "type"),
            @Result(property = "isDeleted", column = "is_deleted"),
    })
    List<Notitification> getNotificationByUserId(Integer userId);

    @SelectProvider(method = "getNotificationById", type = NotificationProvider.class)
    @Results({
            @Result(property = "id", column = "notification_id"),
            @Result(property = "userId", column = "receiver_id"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "isRead", column = "is_read"),
            @Result(property = "isDeleted", column = "is_deleted"),
    })
    Notitification getNotificationById(Integer id);

    @UpdateProvider(method = "updateIsRead", type = NotificationProvider.class)
    boolean updateIsRead(Integer id);

    @UpdateProvider(method = "updateIsDeleted", type = NotificationProvider.class)
    boolean updateIsDeleted(Integer id);

    @Select("Select u.one_signal_id, u.sender_id , u.name , u.user_id ,u.gender ,u.profile_url from dd_users u" +
            " where u.user_id = #{senderID}")
    @Results({
            @Result(property = "id" , column = "user_id"),
            @Result(property = "gender" , column = "gender"),
            @Result(property = "userName" , column = "name"),
            @Result(property = "profileUrl" , column = "profile_url"),
            @Result(property = "oneSignalId",column = "one_signal_id"),
            @Result(property = "senderId", column = "sender_id"),
    })
    User getUserBySenderId(int senderID);

    @Select("SELECT s.task_id , s.title FROM dd_simply_tasks s WHERE s.task_id = #{taskId}   AND s.is_shared = true  AND s.is_deleted = false ")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "title", column = "title"),
    })
    SimplyTask getTaskByShareTaskId(int taskId);


    @Select("SELECT * FROM dd_roles WHERE role_id = #{roleId}  ")
    @Results({
            @Result(property = "id", column = "role_id"),
            @Result(property = "roleType", column = "role_type"),
            @Result(property = "createdDate", column = "created_date")
    })
    Role getRoleById(int roleId);
}
