package com.kshrd.daydo.repository.ShareTaskRepo;

import com.kshrd.daydo.model.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SharesRepository {

    @Update("UPDATE dd_simply_tasks SET is_shared = TRUE WHERE task_id = #{taskId}")
    boolean updateSimplyTaskIsShared(int taskId);

    @Update("UPDATE dd_simply_tasks SET is_shared = TRUE WHERE parent_task_id = #{taskId}")
    boolean updateSimplySubTaskIsShared(int taskId);

    @Select("")
    Integer simplyTaskIsShared();

    // MARK: - Check user and task are used to share or not
    @Select("SELECT EXISTS (SELECT * FROM dd_shares WHERE user_id = #{userId} AND task_id = #{taskId}) :: INTEGER")
    Integer existedShare(SharedTask sharedTask);

    // MARK: - Insert Share if User accept sharing
    @Insert("INSERT INTO dd_shares (shared_date, user_id, task_id, role_id, created_date) VALUES (now(), #{userId}, #{taskId}, #{roleId}, now())")
    boolean insertShareTask(SharedTask sharedTask);

    // Mark: - When a task used to share to a user but that owner delete that user, and then owner add again.
    @Update("UPDATE dd_shares SET is_deleted = FALSE WHERE user_id = #{userId} AND task_id = #{taskId}")
    boolean insertTaskToExistedUser(SharedTask sharedTask);

    @Update("UPDATE dd_shares SET role_id = #{roleId} WHERE user_id = #{userId} AND task_id = #{taskId} AND is_deleted = FALSE")
    boolean updateShareRoleByUserIdAndTaskId(int userId, int taskId, int roleId);

    @Update("UPDATE dd_shares SET is_deleted = TRUE WHERE task_id = #{taskId} AND is_deleted = FALSE")
    boolean deleteShareByShareId(int userId, int taskId);

    @Update("UPDATE dd_shares SET is_deleted = TRUE WHERE task_id = #{taskId} AND is_deleted = FALSE")
    boolean deleteShareByOwner(int userId, int taskId);

    @Update("UPDATE dd_shares SET is_deleted = TRUE WHERE user_id = #{userId} and task_id = #{taskId} AND is_deleted = FALSE")
    boolean deleteShareByUserIdTaskId(int userId, int taskId);
    @Select("select role_id from dd_shares where user_id = #{userId} and task_id = #{taskId} and is_deleted = false")
    int isOwner(int userId, int taskId);

    // MARK: -
    @Select("SELECT * FROM dd_shares WHERE user_id = #{userId} AND is_deleted = false " +
            "ORDER BY created_date desc")// AND role_id = 1")
    @Results({
            @Result(property = "amountOfSharingUser", column = "task_id", one = @One(select = "getAmountOfSharingUser")),
            @Result(property = "id", column = "shared_id"),
            @Result(property = "sharedDate", column = "shared_date"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "taskId", column = "task_id"),
            @Result(property = "roleId", column = "role_id"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "simplyTask", column = "task_id", many = @Many(select = "getTaskByShareTaskId")),
//            @Result(property = "shareSimplyTaskList", column = "task_id", many = @Many(select = "getShareSimplyTaskByTaskId")),
//            @Result(property = "role", column = "role_id", one = @One(select = "getRoleById"))
    })
    List<SharedTask> getAllSharedTaskByUserId(int userId);

    // MARK: -
    @Select("SELECT * from dd_shares where task_id = #{taskId} AND is_deleted = false")
    @Results({
            @Result(property = "id", column = "share_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "shareToUser", column = "user_id", one = @One(select = "getuserByUserId")),
            @Result(property = "taskId", column = "task_id"),
            @Result(property = "roleId", column = "r.role_id")
    })
    List<SharedTask> getShareSimplyTaskByTaskId(int taskId);

    // MARK: -
    @Select("SELECT * FROM dd_simply_tasks WHERE task_id = #{taskId}  AND is_deleted = false ")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "uID", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "isComplete" , column = "is_completed"),
            @Result(property = "isShare",column = "is_shared"),
            @Result(property = "isImportant" ,column = "is_important"),
            @Result(property = "countSimplySubTask", column = "task_id", many = @Many(select = "countSubTaskByTaskId")),
            @Result(property = "simplySubTasks", column = "task_id", many = @Many(select = "getSubTaskListById")),
            @Result(property = "users" , column = "task_id" , many = @Many(select = "getUserByShareTaskID"))
    })
    SimplyTask getTaskByShareTaskId(int taskId);

    // MARK: -
    @Select("SELECT * FROM dd_users u " +
            " inner join dd_shares s on u.user_id = s.user_id " +
            " inner join dd_roles r  on s.role_id = r.role_id " +
            " WHERE task_id = #{taskID} AND s.is_deleted =false")
    @Results({
            @Result(property = "id", column = "user_id"),
            @Result(property = "userName", column = "name"),
            @Result(property = "role" , column = "role_id" , one = @One(select = "getRoleById") )
    })
    List<User> getUserByShareTaskID(int taskID);

    // MARK: -
    @Select("SELECT count(*) FROM dd_shares WHERE task_id = #{taskId} AND is_deleted = false ")
    Integer getAmountOfSharingUser(int taskId);

    // MARK: -
    @Select("SELECT * FROM dd_roles WHERE role_id = #{roleId}  ")
    @Results({
            @Result(property = "id", column = "role_id"),
            @Result(property = "roleType", column = "role_type"),
            @Result(property = "createdDate", column = "created_date")
    })
    Role getRoleById(int roleId);

    // MARK: -


    //Todo get Sub Task
    @Select("SELECT  * from dd_simply_tasks where parent_task_id = #{taskID} AND is_deleted = false")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "parentId", column = "parent_task_id"),
            @Result(property = "isComplete", column = "is_completed"),
            @Result(property = "isImportant", column = "is_important"),
            @Result(property = "createdDate", column = "created_date"),
    })
    List<SimplySubTask> getSubTaskListById(int taskID);

    //Todo create count sub task method
    @Select("SELECT count(*) from dd_simply_tasks WHERE parent_task_id = #{taskID};")
    Integer countSubTaskByTaskId(int taskID);

}




































