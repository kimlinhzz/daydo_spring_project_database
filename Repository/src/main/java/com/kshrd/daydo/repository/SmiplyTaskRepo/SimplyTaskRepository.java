package com.kshrd.daydo.repository.SmiplyTaskRepo;

import com.kshrd.daydo.model.Role;
import com.kshrd.daydo.model.SimplySubTask;
import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SimplyTaskRepository {

    @Insert("INSERT into dd_simply_tasks (user_id, title , note ,  start_date ,  finish_date ,  remind_date ,  is_completed , is_important , is_shared) " +
            "values (#{uID},#{title},#{note},#{startDate},#{finishDate},#{remindDate},#{isComplete},#{isImportant},#{isShare})")
    boolean insertSimplyTask(SimplyTask simplyTask);

    @Insert("INSERT into dd_simply_tasks (user_id , parent_task_id , title , is_completed , is_important) " +
            "values (#{uID},#{parentId},#{title},#{isComplete},#{isImportant})")
    boolean insertSimplySubTask(SimplySubTask simplySubTask);

    //Delete Task By ID
    @Update("UPDATE dd_simply_tasks set is_deleted =true where task_id = #{taskID} AND parent_task_id ISNULL AND is_deleted = false ")
    boolean deleteSimplyTaskById(int taskID);

    //Delete Task By ID
    @Update("UPDATE dd_simply_tasks set is_deleted = true where task_id = #{taskSubID} AND is_deleted = false")
    boolean deleteSimplySubTaskById(int taskSubID);

    @Update("UPDATE dd_simply_tasks set title = #{title} , note = #{note}, start_date = #{startDate} , finish_date = #{finishDate} , " +
            "remind_date = #{remindDate},is_shared = #{isShare},is_completed = #{isComplete},is_archive=#{isArchive} ,is_important = #{isImportant} where task_id = #{id} AND parent_task_id ISNULL")
    boolean updateSimplyTasks(SimplyTask simplyTask);

    @Update("UPDATE dd_simply_tasks set title = #{title}," +
            "is_completed = #{isComplete},is_important = #{isImportant} where task_id = #{id}")
    boolean updateSimplySubTasks(SimplySubTask simplySubTask);


    @Select("SELECT * from dd_simply_tasks " +
            "where user_id = #{userID} AND parent_task_id ISNULL AND is_shared = false AND is_deleted = false " +
            "ORDER BY created_date desc " +
            " limit 1")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "uID", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "isComplete" , column = "is_completed"),
            @Result(property = "isShare",column = "is_shared"),
            @Result(property = "isImportant" ,column = "is_important"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "countSimplySubTask", column = "task_id", many = @Many(select = "countSubTaskByTaskId")),
            @Result(property = "simplySubTasks", column = "task_id", many = @Many(select = "getSubTaskListById"))
    })
    SimplyTask getLastestSimplyTasksByUserId(int userID);

    @Select("SELECT  * from dd_simply_tasks where  user_id = #{userID} AND is_deleted = false AND is_shared = false " +
            "ORDER BY created_date desc " +
            "limit 1")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "parentId", column = "parent_task_id"),
            @Result(property = "isComplete", column = "is_completed"),
            @Result(property = "isImportant", column = "is_important"),
            @Result(property = "createdDate", column = "created_date")})
    SimplySubTask getLastestSimplySubTasksByUserId( int taskID, int userID);

    //Todo Get All Task
    @Select("SELECT * from dd_simply_tasks where user_id = #{userID} AND parent_task_id ISNULL AND is_deleted = false AND is_shared = false")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "uID", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "isComplete" , column = "is_completed"),
            @Result(property = "isShare",column = "is_shared"),
            @Result(property = "isImportant" ,column = "is_important"),
            @Result(property = "isDelete",column = "is_deleted"),
            @Result(property = "startDate" , column = "start_date"),
            @Result(property = "finishDate" , column = "finish_date"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "countSimplySubTask", column = "task_id", many = @Many(select = "countSubTaskByTaskId")),
            @Result(property = "simplySubTasks", column = "task_id", many = @Many(select = "getSubTaskListById"))
    })
    List<SimplyTask> getAllSimplyTasksByUserId(int userID);

    //Todo get all the simply tasks by 'userID' And type like 'today , tomorrow , coming_soon , someday ,schedule'
    @SelectProvider(type = SimplyTaskProvider.class,method = "getSimplyTasks")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "uID", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "isComplete", column = "is_completed"),
            @Result(property = "isShare", column = "is_shared"),
            @Result(property = "isImportant", column = "is_important"),
            @Result(property = "isDelete", column = "is_deleted"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "remindDate", column = "remind_date"),
            @Result(property = "startDate" , column = "start_date"),
            @Result(property = "finishDate" , column = "finish_date"),
            @Result(property = "countSimplySubTask", column = "task_id", many = @Many(select = "countSubTaskByTaskId")),
            @Result(property = "simplySubTasks", column = "task_id", many = @Many(select = "getSubTaskListById"))
    })
    List<SimplyTask> getSimplyTasksByUserIDAndDate(int userID , String type);

    //Todo get Sub Task
    @Select("SELECT  * from dd_simply_tasks where parent_task_id = #{taskID} AND is_deleted = false")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "parentId", column = "parent_task_id"),
            @Result(property = "isComplete", column = "is_completed"),
            @Result(property = "isImportant", column = "is_important"),
            @Result(property = "createdDate", column = "created_date"),
    })
    List<SimplySubTask> getSubTaskListById(int taskID);

    @Select(("SELECT * from dd_simply_tasks" +
            " where task_id = #{taskID} AND is_deleted = false AND parent_task_id ISNULL"))
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "uID", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "note", column = "note"),
            @Result(property = "isComplete", column = "is_completed"),
            @Result(property = "isShare", column = "is_shared"),
            @Result(property = "isImportant", column = "is_important"),
            @Result(property = "isDelete", column = "is_deleted"),
            @Result(property = "startDate" , column = "start_date"),
            @Result(property = "finishDate" , column = "finish_date"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "isArchive" , column = "is_archive"),
            @Result(property = "countSimplySubTask", column = "task_id", many = @Many(select = "countSubTaskByTaskId")),
            @Result(property = "simplySubTasks", column = "task_id", many = @Many(select = "getSubTaskListById")),
            @Result(property = "users" , column = "task_id" , many = @Many(select = "getUserByShareTaskID")),
            @Result(property = "remindDate", column = "remind_date")})
    SimplyTask getSimplyTaskById(int taskID);

    @Select("SELECT * FROM dd_users u " +
            " inner join dd_shares s on u.user_id = s.user_id " +
            " inner join dd_roles r  on s.role_id = r.role_id " +
            " WHERE task_id = #{taskID} AND s.is_deleted =false")
    @Results({
            @Result(property = "id", column = "user_id"),
            @Result(property = "userName", column = "name"),
            @Result(property = "role" , column = "role_id" , one = @One(select = "getRoleById") )
    })
    List<User> getUserByShareTaskID(int taskID);

    @Select("SELECT * FROM dd_roles WHERE role_id = #{roleId}  ")
    @Results({
            @Result(property = "id", column = "role_id"),
            @Result(property = "roleType", column = "role_type"),
            @Result(property = "createdDate", column = "created_date")
    })
    Role getRoleById(int roleId);


    //Todo create count sub task method
    @Select("SELECT count(*) from dd_simply_tasks WHERE parent_task_id = #{taskID} AND is_deleted = false")
    Integer countSubTaskByTaskId(int taskID);


    //Update SimplyTask To Archive
    @Update("UPDATE dd_simply_tasks set is_archive = #{isArchive} where task_id = #{taskID} AND parent_task_id ISNULL")
    boolean updateToArchive(int taskID , boolean isArchive);

    //Delete Task By ID
    @Update("UPDATE dd_simply_tasks set is_deleted = true where task_id = #{taskID} AND is_archive = true AND parent_task_id ISNULL AND is_deleted = false ")
    boolean deleteArchiveById(int taskID);

    @Select("SELECT * from dd_simply_tasks where user_id = #{userID} AND parent_task_id ISNULL AND is_deleted = false AND is_shared = false AND is_archive = true")
    @Results({
            @Result(property = "id", column = "task_id"),
            @Result(property = "uID", column = "user_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "countSimplyTask" , column = "user_id" ,many = @Many(select = "countArchiveByUserId"))
    })
    List<SimplyTask> getAllArchiveByUserId(int userID);

    @Select("SELECT count(*) from dd_simply_tasks WHERE user_id = #{userID} AND is_archive = true AND is_deleted = false;")
    Integer countArchiveByUserId(int userID);

}

