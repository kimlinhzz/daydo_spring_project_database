package com.kshrd.daydo.repository.SmiplyTaskRepo;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class SimplyTaskProvider {

    public String getSimplyTasks(@Param("userID") int userID, @Param("type") String type ) {
        return new SQL() {{
            SELECT("*");
            FROM("dd_simply_tasks");
            if (type.equals("today")){
                WHERE("user_id = #{userID} AND finish_date ISNULL AND start_date ISNULL" +
                        " AND parent_task_id ISNULL AND is_shared = false AND is_deleted = false AND is_archive = false " +
                        "And (remind_date::date = now()::date OR remind_date ISNULL)");
                ORDER_BY("is_important desc , created_date desc");
            }
            if(type.equals("tomorrow")){
                WHERE("user_id = #{userID} AND finish_date ISNULL AND start_date ISNULL  AND parent_task_id ISNULL AND is_shared = false AND is_deleted = false AND is_archive = false " +
                        "And (remind_date ::timestamp::date = now()::date + interval '1 day')");
                ORDER_BY("is_important desc , created_date desc");
            }
            if (type.equals("someday")) {
                WHERE("user_id = #{userID} AND finish_date ISNULL AND start_date ISNULL  AND parent_task_id ISNULL AND is_shared = false AND is_deleted = false AND is_archive = false " +
                        "And (remind_date ::timestamp::date > now() +( interval '1 week'))");
                ORDER_BY("is_important desc , created_date desc");
            }
            if (type.equals("coming_soon")) {
                WHERE(" user_id = #{userID} AND finish_date ISNULL AND start_date ISNULL  AND parent_task_id ISNULL AND is_shared = false AND is_deleted = false AND is_archive = false " +
                        "And (remind_date ::timestamp::date >= now()::date +(interval '3 day'))");
                ORDER_BY("is_important desc , created_date desc");
            }
            if (type.equals("schedule")){
                WHERE("user_id = #{userID}  AND parent_task_id ISNULL AND is_shared = false AND is_deleted = false AND is_archive = false " +
                        "And remind_date ::timestamp::date between start_date ::timestamp::date AND  finish_date ::timestamp::date " +
                        "AND finish_date ::timestamp::date >=now()");
                ORDER_BY("is_important desc , created_date desc");
            }
        }}.toString();
    }
}
