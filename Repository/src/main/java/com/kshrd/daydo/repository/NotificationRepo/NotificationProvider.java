package com.kshrd.daydo.repository.NotificationRepo;

import com.kshrd.daydo.model.Notitification;
import org.apache.ibatis.jdbc.SQL;

public class NotificationProvider {
    public String insertNotification(Notitification notification) {
        return new SQL() {{
            INSERT_INTO("dd_notifications");
            VALUES("receiver_id", "#{userId}");
            VALUES("sender_id", "#{senderId}");
            VALUES("task_id", "#{taskId}");
            VALUES("role_id", "#{roleId}");
            VALUES("type", "#{type}");
            VALUES("title", "#{title}");
            VALUES("description", "#{description}");
        }}.toString();
    }

    public String getAllNotifications() {
        return new SQL() {{
            SELECT("*");
            FROM("dd_notifications");
        }}.toString();
    }

    public String getNotificationByUserId(Integer userId) {
        return new SQL() {{
            SELECT("*");
            FROM("dd_notifications");
            WHERE("receiver_id = #{userId} AND is_deleted = false");
            ORDER_BY("created_date desc");
        }}.toString();
    }

    public String getNotificationById(Integer id) {
        return new SQL() {{
            SELECT("*");
            FROM("dd_notifications");
            WHERE("notification_id = #{id} AND is_deleted = false");
        }}.toString();
    }

    public String updateIsRead(Integer id) {
        return new SQL() {{
            UPDATE("dd_notifications");
            SET("is_read = true");
            WHERE("notification_id = #{id}");
        }}.toString();
    }

    public String updateIsDeleted(Integer id) {
        return new SQL() {{
            UPDATE("dd_notifications");
            SET("is_deleted = true");
            WHERE("notification_id = #{id}");
        }}.toString();
    }
}
