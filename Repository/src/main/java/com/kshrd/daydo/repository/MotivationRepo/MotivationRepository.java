package com.kshrd.daydo.repository.MotivationRepo;

import com.kshrd.daydo.model.Motivation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MotivationRepository {

    @Select("SELECT * FROM dd_motivations")
    @Results({
            @Result(property = "id", column = "motivation_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "isDeleted", column = "is_deleted"),
            @Result(property = "imageUrl", column = "image")
    })
    List<Motivation> getAllMotivations();


    @Insert("INSERT INTO dd_motivations(motivation_id, title, description, image, is_deleted, created_date) " +
            "VALUES (#{id}, #{title}, #{description}, NULL ,'f', CURRENT_TIMESTAMP);")
    boolean insertMotivation(Motivation motivation);
}
