package com.kshrd.daydo.repository.UserRepo;

import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.model.SimplySubTask;
import com.kshrd.daydo.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UserRepository {

    @Insert("Insert Into dd_users(name , gender , email , profile_url ,one_signal_id , sender_id , access_token) Values " +
            "(#{userName}, #{gender} , #{email},#{profileUrl},#{oneSignalId} ,#{senderId},#{accessToken})" )
    boolean addUser(User user);

    @Update("Update dd_users set name = #{userName} , profile_url = #{profileUrl} where user_id = #{id}")
    boolean updateUser(User user);

    @Update("Update dd_users set is_delete =true  where user_id = #{id}")
    boolean deleteUser(int userID);

    @Select("Select * From dd_simply_tasks where user_id = #{userID} And where is_completed = true And where is_deleted = false")
        @Results({
                @Result(property = "id" , column = "task_id"),
                @Result(property = "uID" , column = "user_id"),
                @Result(property = "title", column = "title"),
                @Result(property = "note", column = "note"),
                @Result(property = "subTaskList" ,column = "task_id",many = @Many(select = "getSubTaskListById")),
                @Result(property = "createdDate" , column = "created_date")
        })
    List<SimplyTask> getCompleteSimplyTask(int userID);

    @Select("SELECT  * from dd_simply_tasks where parent_task_id =#{taskID} AND is_deleted = false")
    @Results({
            @Result(property = "id" , column = "task_id"),
            @Result(property = "title" , column = "title"),
            @Result(property = "parentId" , column = "parent_task_id"),
            @Result(property = "isCompleted" , column = "is_completed"),
            @Result(property = "createdDate" , column = "created_date")
    })
    List<SimplySubTask> getSubTaskListById(int taskID);


    @Select("Select * from dd_users where access_token = #{accessToken}")
    @Results({
            @Result(property = "id" , column = "user_id"),
            @Result(property = "gender" , column = "gender"),
            @Result(property = "userName" , column = "name"),
            @Result(property = "email" , column = "email"),
            @Result(property = "profileUrl" , column = "profile_url"),
            @Result(property = "oneSignalId",column = "one_signal_id"),
            @Result(property = "senderId", column = "sender_id"),
            @Result(property = "accessToken" , column = "access_token")
    })
    User loginUser(String accessToken);

    @Select("Select * from dd_users where user_id = #{userID}")
    @Results({
            @Result(property = "id" , column = "user_id"),
            @Result(property = "gender" , column = "gender"),
            @Result(property = "userName" , column = "name"),
            @Result(property = "email" , column = "email"),
            @Result(property = "profileUrl" , column = "profile_url"),
            @Result(property = "oneSignalId",column = "one_signal_id"),
            @Result(property = "senderId", column = "sender_id"),
            @Result(property = "accessToken" , column = "access_token")
    })
    User getUserById(int userID);

    @Select("Select u.one_signal_id, u.sender_id , u.name , u.user_id ,u.gender ,u.profile_url from dd_users u" +
            " where u.sender_id = #{senderID}")
    @Results({
            @Result(property = "id" , column = "user_id"),
            @Result(property = "gender" , column = "gender"),
            @Result(property = "userName" , column = "name"),
            @Result(property = "profileUrl" , column = "profile_url"),
            @Result(property = "oneSignalId",column = "one_signal_id"),
            @Result(property = "senderId", column = "sender_id"),
    })
    User findUserBySenderId(String senderID);

    @Select("Select * from dd_users")
    @Results({
            @Result(property = "id" , column = "user_id"),
            @Result(property = "gender" , column = "gender"),
            @Result(property = "userName" , column = "name"),
            @Result(property = "email" , column = "email"),
            @Result(property = "profileUrl" , column = "profile_url"),
            @Result(property = "oneSignalId",column = "one_signal_id"),
            @Result(property = "senderId", column = "sender_id"),
            @Result(property = "accessToken" , column = "access_token")
    })
  List<User> getAllUsers();

    @Select("Select u.access_token from dd_users u where u.access_token = #{accessToken}")
    boolean checkUser(String accessToken);
}
