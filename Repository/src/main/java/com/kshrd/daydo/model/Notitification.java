package com.kshrd.daydo.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;

public class Notitification {

    Integer id;
    Integer userId;
    Integer senderId;
    Integer taskId;
    Integer roleId;
    Integer type;
    String title;
    String description;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp createdDate;
    boolean isRead;
    boolean isDeleted;

    User user;
    SimplyTask simplyTask;
    Role role;

    public Notitification() {
    }

    public Notitification(Integer userId, Integer senderId, Integer taskId, Integer roleId, String title, String description, Timestamp createdDate, boolean isRead, boolean isDeleted) {
        this.userId = userId;
        this.senderId = senderId;
        this.taskId = taskId;
        this.roleId = roleId;
        this.title = title;
        this.description = description;
        this.createdDate = createdDate;
        this.isRead = isRead;
        this.isDeleted = isDeleted;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SimplyTask getSimplyTask() {
        return simplyTask;
    }

    public void setSimplyTask(SimplyTask simplyTask) {
        this.simplyTask = simplyTask;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public String toString() {
        return "Notitification{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", createdDate=" + createdDate +
                ", isRead=" + isRead +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
