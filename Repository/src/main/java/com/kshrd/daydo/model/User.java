package com.kshrd.daydo.model;

public class User {

    int id;
    String gender;
    String userName;
    String email;
    String profileUrl;
    String oneSignalId;
    String senderId;
    String accessToken;
    Role role;
    public User() {
    }

    public User(String gender, String userName, String email, String profileUrl, String oneSignalId, String senderId, String accessToken, Role role) {
        this.gender = gender;
        this.userName = userName;
        this.email = email;
        this.profileUrl = profileUrl;
        this.oneSignalId = oneSignalId;
        this.senderId = senderId;
        this.accessToken = accessToken;
        this.role = role;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getOneSignalId() {
        return oneSignalId;
    }

    public void setOneSignalId(String oneSignalId) {
        this.oneSignalId = oneSignalId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
