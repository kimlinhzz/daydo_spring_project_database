package com.kshrd.daydo.model.ReportModel;

public class ProgressReport {
    Integer progressId;
    Double progressPercentage;

    public ProgressReport() {
    }

    public ProgressReport(Integer progressId, Double progressPercentage) {
        this.progressId = progressId;
        this.progressPercentage = progressPercentage;
    }

    public Integer getProgressId() {
        return progressId;
    }

    public void setProgressId(Integer progressId) {
        this.progressId = progressId;
    }

    public Double getProgressPercentage() {
        return progressPercentage;
    }

    public void setProgressPercentage(Double progressPercentage) {
        this.progressPercentage = progressPercentage;
    }

    @Override
    public String toString() {
        return "ProgressReport{" +
                "progressId=" + progressId +
                ", progressPercentage=" + progressPercentage +
                '}';
    }
}
