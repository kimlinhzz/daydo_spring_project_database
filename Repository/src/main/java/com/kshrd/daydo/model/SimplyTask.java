package com.kshrd.daydo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;
import java.util.List;

public class SimplyTask {

    Integer id;
    Integer uID;
    Integer parentId;
    String title;
    String note;
    Boolean isShare;
    Boolean isComplete;
    Boolean isImportant;
    Boolean isDelete;
    Boolean isArchive;
    Integer countSimplyTask;
    Integer countSimplySubTask;
    List<SimplySubTask> simplySubTasks;
    List<User> users;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp startDate;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp finishDate;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp remindDate;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp createdDate;

    public SimplyTask() {
    }

    public SimplyTask(Integer uID, Integer parentId, String title, String note, Boolean isShare, Boolean isComplete, Boolean isImportant, Boolean isDelete, Boolean isArchive, Integer countSimplyTask, Integer countSimplySubTask, List<SimplySubTask> simplySubTasks, List<User> users, Timestamp startDate, Timestamp finishDate, Timestamp remindDate, Timestamp createdDate) {
        this.uID = uID;
        this.parentId = parentId;
        this.title = title;
        this.note = note;
        this.isShare = isShare;
        this.isComplete = isComplete;
        this.isImportant = isImportant;
        this.isDelete = isDelete;
        this.isArchive = isArchive;
        this.countSimplyTask = countSimplyTask;
        this.countSimplySubTask = countSimplySubTask;
        this.simplySubTasks = simplySubTasks;
        this.users = users;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.remindDate = remindDate;
        this.createdDate = createdDate;
    }

    public Boolean getArchive() {
        return isArchive;
    }

    public void setArchive(Boolean archive) {
        isArchive = archive;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Integer getCountSimplySubTask() {
        return countSimplySubTask;
    }

    public void setCountSimplySubTask(Integer countSimplySubTask) {
        this.countSimplySubTask = countSimplySubTask;
    }

    public Integer getCountSimplyTask() {
        return countSimplyTask;
    }

    public void setCountSimplyTask(Integer countSimplyTask) {
        this.countSimplyTask = countSimplyTask;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getuID() {
        return uID;
    }

    public void setuID(Integer uID) {
        this.uID = uID;
    }

    public Boolean getComplete() {
        return isComplete;
    }

    public void setComplete(Boolean complete) {
        isComplete = complete;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public List<SimplySubTask> getSimplySubTasks() {
        return simplySubTasks;
    }

    public void setSimplySubTasks(List<SimplySubTask> simplySubTasks) {
        this.simplySubTasks = simplySubTasks;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Timestamp finishDate) {
        this.finishDate = finishDate;
    }

    public Timestamp getRemindDate() {
        return remindDate;
    }

    public void setRemindDate(Timestamp remindDate) {
        this.remindDate = remindDate;
    }

    public Boolean getShare() {
        return isShare;
    }

    public void setShare(Boolean share) {
        isShare = share;
    }


    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
