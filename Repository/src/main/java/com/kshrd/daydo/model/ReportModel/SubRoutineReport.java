package com.kshrd.daydo.model.ReportModel;

public class SubRoutineReport {
    Integer subTaskId;
    Integer parentTaskId;
    String title;
    ProgressReport progressReport;

    public SubRoutineReport() {
    }

    public SubRoutineReport(Integer subTaskId, Integer parentTaskId, String title, ProgressReport progressReport) {
        this.subTaskId = subTaskId;
        this.parentTaskId = parentTaskId;
        this.title = title;
        this.progressReport = progressReport;
    }

    public Integer getSubTaskId() {
        return subTaskId;
    }

    public void setSubTaskId(Integer subTaskId) {
        this.subTaskId = subTaskId;
    }

    public Integer getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(Integer parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public void setTitle(String title){this.title = title;}
    public String getTitle(){return  title;}

    public ProgressReport getProgressReport() {
        return progressReport;
    }

    public void setProgressReport(ProgressReport progressReport) {
        this.progressReport = progressReport;
    }

    @Override
    public String toString() {
        return "SubRoutineReport{" +
                "subTaskId=" + subTaskId +
                ", parentTaskId=" + parentTaskId +
                ", title='" + title + '\'' +
                ", progressReport=" + progressReport +
                '}';
    }
}
