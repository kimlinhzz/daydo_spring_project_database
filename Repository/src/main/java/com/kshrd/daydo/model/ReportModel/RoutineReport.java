package com.kshrd.daydo.model.ReportModel;

import java.util.List;

public class RoutineReport {
    Integer taskId;
    String title;
    String note;
    Integer totalSubTask;
    Double totalSubTaskProgressPercentage;
    List<SubRoutineReport> subRoutineReportList;

    public RoutineReport() {
    }

    public RoutineReport(Integer taskId, String title, String note, Integer totalSubTask, Double totalSubTaskProgressPercentage, List<SubRoutineReport> subRoutineReportList) {
        this.taskId = taskId;
        this.title = title;
        this.note = note;
        this.totalSubTask = totalSubTask;
        this.totalSubTaskProgressPercentage = totalSubTaskProgressPercentage;
        this.subRoutineReportList = subRoutineReportList;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getTotalSubTask() {
        return totalSubTask;
    }

    public void setTotalSubTask(Integer totalSubTask) {
        this.totalSubTask = totalSubTask;
    }

    public Double getTotalSubTaskProgressPercentage() {
        return totalSubTaskProgressPercentage;
    }

    public void setTotalSubTaskProgressPercentage(Double totalSubTaskProgressPercentage) {
        this.totalSubTaskProgressPercentage = totalSubTaskProgressPercentage;
    }

    public List<SubRoutineReport> getSubRoutineReportList() {
        return subRoutineReportList;
    }

    public void setSubRoutineReportList(List<SubRoutineReport> subRoutineReportList) {
        this.subRoutineReportList = subRoutineReportList;
    }

    @Override
    public String toString() {
        return "RoutineReport{" +
                "taskId=" + taskId +
                ", title='" + title + '\'' +
                ", note='" + note + '\'' +
                ", totalSubTask=" + totalSubTask +
                ", totalSubTaskProgressPercentage=" + totalSubTaskProgressPercentage +
                ", subRoutineReportList=" + subRoutineReportList +
                '}';
    }
}
