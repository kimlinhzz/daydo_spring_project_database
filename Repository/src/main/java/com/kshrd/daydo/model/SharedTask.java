package com.kshrd.daydo.model;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.List;

public class SharedTask {
    Integer amountOfSharingUser;
    Integer id;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp sharedDate;
    Integer userId;
    Integer taskId;
    Integer roleId;
    boolean isDeleted;
    Timestamp createdDate;
    SimplyTask simplyTask;
    Role role;
    User shareToUser;

    List<SimplyTask> shareSimplyTaskList;

    public SharedTask() {
    }

    public SharedTask(Integer amountOfSharingUser, Timestamp sharedDate, Integer userId, Integer taskId, Integer roleId, boolean isDeleted, Timestamp createdDate, SimplyTask simplyTask, Role role, User shareToUser, List<SimplyTask> shareSimplyTaskList) {
        this.amountOfSharingUser = amountOfSharingUser;
        this.sharedDate = sharedDate;
        this.userId = userId;
        this.taskId = taskId;
        this.roleId = roleId;
        this.isDeleted = isDeleted;
        this.createdDate = createdDate;
        this.simplyTask = simplyTask;
        this.role = role;
        this.shareToUser = shareToUser;
        this.shareSimplyTaskList = shareSimplyTaskList;
    }

    public Integer getAmountOfSharingUser() {
        return amountOfSharingUser;
    }

    public void setAmountOfSharingUser(Integer amountOfSharingUser) {
        this.amountOfSharingUser = amountOfSharingUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getSharedDate() {
        return sharedDate;
    }

    public void setSharedDate(Timestamp sharedDate) {
        this.sharedDate = sharedDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public SimplyTask getSimplyTask() {
        return simplyTask;
    }

    public void setSimplyTask(SimplyTask simplyTask) {
        this.simplyTask = simplyTask;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User getShareToUser() {
        return shareToUser;
    }

    public void setShareToUser(User shareToUser) {
        this.shareToUser = shareToUser;
    }

    public List<SimplyTask> getShareSimplyTaskList() {
        return shareSimplyTaskList;
    }

    public void setShareSimplyTaskList(List<SimplyTask> shareSimplyTaskList) {
        this.shareSimplyTaskList = shareSimplyTaskList;
    }
}
