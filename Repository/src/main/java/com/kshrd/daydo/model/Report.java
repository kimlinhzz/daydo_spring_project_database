package com.kshrd.daydo.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.List;

public class Report {
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp progressDate;
    Integer totalSubRoutineTask;
    RoutineTask rountineTaskList;
    Double totalPercentage;

    public Report() {
    }

    public Report(Timestamp progressDate, Integer totalRoutineTask, RoutineTask rountineTaskList, Double totalPercentage) {
        this.progressDate = progressDate;
        this.totalSubRoutineTask = totalRoutineTask;
        this.rountineTaskList = rountineTaskList;
        this.totalPercentage = totalPercentage;
    }

    public Timestamp getProgressDate() {
        return progressDate;
    }

    public void setProgressDate(Timestamp progressDate) {
        this.progressDate = progressDate;
    }

    public Integer getTotalSubRoutineTask() {
        return totalSubRoutineTask;
    }

    public void setTotalSubRoutineTask(Integer totalSubRoutineTask) {
        this.totalSubRoutineTask = totalSubRoutineTask;
    }

    public RoutineTask getRountineTaskList() {
        return rountineTaskList;
    }

    public void setRountineTaskList(RoutineTask rountineTaskList) {
        this.rountineTaskList = rountineTaskList;
    }

    public Double getTotalPercentage() {
        return totalPercentage;
    }

    public void setTotalPercentage(Double totalPercentage) {
        this.totalPercentage = totalPercentage;
    }

    @Override
    public String toString() {
        return "Report{" +
                "progressDate=" + progressDate +
                ", totalSubRoutineTask=" + totalSubRoutineTask +
                ", rountineTaskList=" + rountineTaskList +
                ", totalPercentage=" + totalPercentage +
                '}';
    }
}
