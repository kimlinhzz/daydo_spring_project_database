package com.kshrd.daydo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Motivation {

    @JsonProperty(value = "id",access = JsonProperty.Access.READ_ONLY)
    Integer id;
    @JsonProperty(value = "title",access = JsonProperty.Access.READ_WRITE)
    String title;
    @JsonProperty(value = "description",access = JsonProperty.Access.READ_WRITE)
    String description;
    @JsonProperty(value = "imageUrl",access = JsonProperty.Access.READ_WRITE)
    String imageUrl;
    @JsonProperty(value = "deleted",access = JsonProperty.Access.READ_ONLY)
    Boolean isDeleted;

    public Motivation() {
    }

    public Motivation(String title, String description, String imageUrl, Boolean isDeleted) {
        this.title = title;
        this.description = description;
        this.isDeleted = isDeleted;
        this.imageUrl = imageUrl;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
