package com.kshrd.daydo.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;

public class Progress {

    Integer id;
    Integer taskId;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp progressDate;
    Double progressPercentage;
    Boolean isDeleted;
    Timestamp createdDate;

    public Progress(){}

    public Progress(Integer taskId, Timestamp progressDate, Double progressPercentage, Boolean isDeleted, Timestamp createdDate) {
        this.taskId = taskId;
        this.progressDate = progressDate;
        this.progressPercentage = progressPercentage;
        this.isDeleted = isDeleted;
        this.createdDate = createdDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Timestamp getProgressDate() {
        return progressDate;
    }

    public void setProgressDate(Timestamp progressDate) {
        this.progressDate = progressDate;
    }

    public Double getProgressPercentage() {
        return progressPercentage;
    }

    public void setProgressPercentage(Double progressPercentage) {
        this.progressPercentage = progressPercentage;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "Progress{" +
                "id=" + id +
                ", taskId=" + taskId +
                ", progressDate=" + progressDate +
                ", progressPercentage=" + progressPercentage +
                ", isDeleted=" + isDeleted +
                ", createdDate=" + createdDate +
                '}';
    }
}
