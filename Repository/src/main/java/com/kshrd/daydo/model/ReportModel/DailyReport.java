package com.kshrd.daydo.model.ReportModel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kshrd.daydo.model.RoutineTask;

import java.sql.Timestamp;
import java.util.List;

public class DailyReport {
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss" , timezone = "Asia/Phnom_Penh")
    Timestamp progressDate;
    Integer totalTask;
    Integer totalSubTask;
    Double totalTaskProgressPercentage;
    List<RoutineReport> routineTaskList;

    public DailyReport() {
    }

    public DailyReport(Timestamp progressDate, Integer totalTask, Integer totalSubTask, Double totalTaskProgressPercentage, List<RoutineReport> routineTaskList) {
        this.progressDate = progressDate;
        this.totalTask = totalTask;
        this.totalSubTask = totalSubTask;
        this.totalTaskProgressPercentage = totalTaskProgressPercentage;
        this.routineTaskList = routineTaskList;
    }

    public Timestamp getProgressDate() {
        return progressDate;
    }

    public void setProgressDate(Timestamp progressDate) {
        this.progressDate = progressDate;
    }

    public Integer getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(Integer totalTask) {
        this.totalTask = totalTask;
    }

    public Integer getTotalSubTask() {
        return totalSubTask;
    }

    public void setTotalSubTask(Integer totalSubTask) {
        this.totalSubTask = totalSubTask;
    }

    public Double getTotalTaskProgressPercentage() {
        return totalTaskProgressPercentage;
    }

    public void setTotalTaskProgressPercentage(Double totalTaskProgressPercentage) {
        this.totalTaskProgressPercentage = totalTaskProgressPercentage;
    }

    public List<RoutineReport> getRoutineTaskList() {
        return routineTaskList;
    }

    public void setRoutineTaskList(List<RoutineReport> routineTaskList) {
        this.routineTaskList = routineTaskList;
    }

    @Override
    public String toString() {
        return "DailyReport{" +
                "progressDate=" + progressDate +
                ", totalTask=" + totalTask +
                ", totalSubTask=" + totalSubTask +
                ", totalTaskProgressPercentage=" + totalTaskProgressPercentage +
                ", routineTaskList=" + routineTaskList +
                '}';
    }
}
