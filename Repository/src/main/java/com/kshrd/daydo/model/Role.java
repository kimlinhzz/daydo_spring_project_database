package com.kshrd.daydo.model;

import java.sql.Timestamp;

public class Role {

    Integer id;
    String roleType;
    Timestamp createdDate;

    public Role() {
    }

    public Role(String roleType, Timestamp createdDate) {
        this.roleType = roleType;
        this.createdDate = createdDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }
}
