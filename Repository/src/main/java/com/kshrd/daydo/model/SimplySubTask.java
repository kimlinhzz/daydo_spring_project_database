package com.kshrd.daydo.model;

import java.sql.Timestamp;

public class SimplySubTask {

    Integer id;
    Integer uID;
    Integer parentId;
    String title;
    Boolean isComplete;
    Boolean isDelete;
    Boolean isImportant;
    Timestamp createdDate;


    public SimplySubTask(){}

    public SimplySubTask(Integer uID, Integer parentId, String title, Boolean isComplete, Boolean isDelete, Boolean isImportant, Timestamp createdDate) {
        this.uID = uID;
        this.parentId = parentId;
        this.title = title;
        this.isComplete = isComplete;
        this.isDelete = isDelete;
        this.isImportant = isImportant;
        this.createdDate = createdDate;
    }


    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public Integer getuID() {
        return uID;
    }

    public void setuID(Integer uID) {
        this.uID = uID;
    }

    public Integer getId() {
        return id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getComplete() {
        return isComplete;
    }

    public void setComplete(Boolean complete) {
        isComplete = complete;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }



    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
