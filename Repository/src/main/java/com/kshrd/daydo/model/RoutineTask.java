package com.kshrd.daydo.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.List;

public class RoutineTask {

    Integer id;
    Integer userId;
    Double totalProgress;
    String title;
    String note;
    List<RoutineSubTask> routineSubTasks;
    Timestamp createdDate;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Asia/Phnom_Penh")
    Timestamp remindDate;
    Boolean isImportant;
    Boolean isDeleted;

    // MARK: - Seakthong
    List<RoutineSubTask> rountineSubTaskList;
    List<RoutineTask> rountineTaskList;

    public Double getTotalProgress() {
        return totalProgress;
    }

    public void setTotalProgress(Double totalProgress) {
        this.totalProgress = totalProgress;
    }

    public List<RoutineTask> getRountineTaskList() {
        return rountineTaskList;
    }

    public void setRountineTaskList(List<RoutineTask> rountineTaskList) {
        this.rountineTaskList = rountineTaskList;
    }

    public RoutineTask(){}

    public List<RoutineSubTask> getRountineSubTaskList() {
        return rountineSubTaskList;
    }

    public void setRountineSubTaskList(List<RoutineSubTask> rountineSubTaskList) {
        this.rountineSubTaskList = rountineSubTaskList;
    }

    public RoutineTask(Integer userId, Double totalProgress, String title, String note, Timestamp createdDate, Timestamp remindDate, Boolean isDeleted, List<RoutineSubTask> rountineSubTaskList, List<RoutineTask> rountineTaskList) {
        this.userId = userId;
        this.totalProgress = totalProgress;
        this.title = title;
        this.note = note;
        this.routineSubTasks = routineSubTasks;
        this.createdDate = createdDate;
        this.remindDate = remindDate;
        this.isImportant = isImportant;
        this.isDeleted = isDeleted;
        this.rountineSubTaskList = rountineSubTaskList;
        this.rountineTaskList = rountineTaskList;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<RoutineSubTask> getRoutineSubTasks() {
        return routineSubTasks;
    }

    public void setRoutineSubTasks(List<RoutineSubTask> routineSubTasks) {
        this.routineSubTasks = routineSubTasks;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getRemindDate() {
        return remindDate;
    }

    public void setRemindDate(Timestamp remindDate) {
        this.remindDate = remindDate;
    }

    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
