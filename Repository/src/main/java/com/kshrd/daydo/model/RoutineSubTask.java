package com.kshrd.daydo.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;

public class RoutineSubTask {

    Integer id;
    Integer userId;
    Integer parentId;
    String title;
    String note;
    Progress progress;
    Boolean isComplete;
    Boolean isDeleted;
    Timestamp createdDate;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Asia/Phnom_Penh")
    Timestamp remindDate;
    Boolean isImportant;

    public RoutineSubTask(){}

    public RoutineSubTask(Integer parentId, Integer userId, String title, String note, Progress progress, Boolean isComplete, Boolean isDeleted,Timestamp remindDate,Boolean isImportant) {
        this.parentId = parentId;
        this.userId = userId;
        this.title = title;
        this.note = note;
        this.progress = progress;
        this.isComplete = isComplete;
        this.isDeleted = isDeleted;
        this.remindDate = remindDate;
        this.isImportant = isImportant;
    }

    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public Timestamp getRemindDate() {
        return remindDate;
    }

    public void setRemindDate(Timestamp remindDate) {
        this.remindDate = remindDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Progress getProgress() {
        return progress;
    }

    public void setProgress(Progress progress) {
        this.progress = progress;
    }

    public Boolean getComplete() {
        return isComplete;
    }

    public void setComplete(Boolean complete) {
        isComplete = complete;
    }

    public Boolean getDelete() {
        return isDeleted;
    }

    public void setDelete(Boolean delete) {
        isDeleted = delete;
    }


    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
