package com.kshrd.daydo.service.RoutineTaskService;

import com.kshrd.daydo.model.Progress;
import com.kshrd.daydo.model.RoutineSubTask;
import com.kshrd.daydo.model.RoutineTask;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

public interface RoutineTaskService {

    RoutineSubTask getLastestRoutineSubTaskById(int taskID, int userID);
    RoutineTask getLastestRoutineTaskById(int userID);
    Progress getProgressById(int progressID);
    RoutineTask getRoutineTaskById(int taskID);
    RoutineSubTask getRoutineSubTaskById(int taskID);
    boolean insertRoutineTask(RoutineTask routineTask);
    boolean insertRoutineSubTask(RoutineSubTask routineSubTask);
    boolean deleteRoutineTask(int taskId);
    boolean deleteRoutineSubTask(int taskId);
    boolean updateRoutineTask(RoutineTask routineTask);
    boolean updateRoutineSubTask(RoutineSubTask routineSubTask);
    boolean insertProgresForNewDay(int taskSubID) throws SQLIntegrityConstraintViolationException;
//    boolean insertRoutineSubTaskProgress(Progress progress);
    boolean updateRoutineSubTaskProgress(int progressId, double progressPercentage);
    List<RoutineTask> getAllRoutineTasksByUserId(int userID);
    Progress getProgressByTaskId(int taskID);
    List<RoutineSubTask> getRoutineSubTasksByParentId(int taskID);
    Progress getProgressByTaskID(int taskID);
}
