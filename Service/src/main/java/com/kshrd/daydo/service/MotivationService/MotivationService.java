package com.kshrd.daydo.service.MotivationService;

import com.kshrd.daydo.model.Motivation;

import java.util.List;

public interface MotivationService {

    List<Motivation> getAllMotivations();
    boolean insertMotivation(Motivation motivation);

}
