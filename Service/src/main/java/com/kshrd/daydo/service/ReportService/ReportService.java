package com.kshrd.daydo.service.ReportService;

import com.kshrd.daydo.model.Report;
import com.kshrd.daydo.model.ReportModel.DailyReport;
import com.kshrd.daydo.model.RoutineTask;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

public interface ReportService {
    List<DailyReport> getDailyReportOnDate(int userId, Timestamp fromDate, Timestamp toDate);
}
