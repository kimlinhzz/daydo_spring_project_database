package com.kshrd.daydo.service.SimplyTaskService;

import com.kshrd.daydo.model.SimplySubTask;
import com.kshrd.daydo.model.SimplyTask;
import org.springframework.stereotype.Service;

import java.util.List;


public interface SimplyTaskService {

    boolean insertSimplyTask(SimplyTask simplyTask);
    boolean insertSimplySubTask(SimplySubTask simplySubTask);
    boolean deleteSimplyTaskById(int taskID);
    boolean deleteSimplySubTaskById(int taskSubID);
    boolean updateSimplyTasks(SimplyTask simplyTask);
    boolean updateSimplySubTasks(SimplySubTask simplySubTask);
    boolean updateToArchive(int taskID , boolean isArchive);
    boolean deleteArchiveById(int taskID);

    List<SimplyTask> getAllArchiveByUserId(int userID);
    List<SimplyTask> getSimplyTasksByUserIdAndDate(int userID , String type);

    SimplyTask getSimplyTaskById(int taskID);
    SimplyTask getLastestSimplyTasksByUserId(int userID);
    SimplySubTask getLastestSimplySubTasksByUserId(int taskID ,int userID);
}
