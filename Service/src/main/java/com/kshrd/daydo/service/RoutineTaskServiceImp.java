package com.kshrd.daydo.service;

import com.kshrd.daydo.model.Progress;
import com.kshrd.daydo.model.RoutineSubTask;
import com.kshrd.daydo.model.RoutineTask;
import com.kshrd.daydo.repository.RoutineTaskRepo.RoutineTaskRepository;
import com.kshrd.daydo.service.RoutineTaskService.RoutineTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

@Service
public class RoutineTaskServiceImp implements RoutineTaskService {

    @Autowired
    RoutineTaskRepository routineTaskRepository;

    @Override
    public RoutineSubTask getLastestRoutineSubTaskById(int taskID, int userID) {
        return routineTaskRepository.getLastestRoutineSubTaskById(taskID , userID);
    }

    @Override
    public RoutineTask getLastestRoutineTaskById(int userID) {
        return routineTaskRepository.getLastestRoutineTaskById(userID);
    }

    @Override
    public Progress getProgressById(int progressID) {
        return routineTaskRepository.getProgressById(progressID);
    }

    @Override
    public RoutineTask getRoutineTaskById(int taskID) {
        return routineTaskRepository.getRoutineTaskById(taskID);
    }

    @Override
    public RoutineSubTask getRoutineSubTaskById(int taskID) {
        return routineTaskRepository.getRoutineSubTaskById(taskID);
    }

    @Override
    public boolean insertRoutineTask(RoutineTask routineTask) {
        return routineTaskRepository.insertRoutineTask(routineTask);
    }

    @Override
    public boolean insertRoutineSubTask(RoutineSubTask routineSubTask) {
        return routineTaskRepository.insertRoutineSubTask(routineSubTask);
    }

    @Override
    public boolean deleteRoutineTask(int taskId) {
        return routineTaskRepository.deleteRoutineTask(taskId);
    }

    @Override
    public boolean deleteRoutineSubTask(int taskId) {
        return routineTaskRepository.deleteRoutineSubTask(taskId);
    }

    @Override
    public boolean updateRoutineTask(RoutineTask routineTask) {
        return routineTaskRepository.updateRoutineTask(routineTask);
    }

    @Override
    public boolean updateRoutineSubTask(RoutineSubTask routineSubTask) {
        return routineTaskRepository.updateRoutineSubTask(routineSubTask);
    }

    @Override
    public boolean insertProgresForNewDay(int taskSubID) throws SQLIntegrityConstraintViolationException {
        return routineTaskRepository.insertProgressForNewDay(taskSubID);
    }

//    @Override
//    public boolean insertRoutineSubTaskProgress(Progress progress) {
//        return routineTaskRepository.insertRoutineSubTaskProgress(progress);
//    }

    @Override
    public boolean updateRoutineSubTaskProgress(int progressId, double progressPercentage) {
        return routineTaskRepository.updateRoutineSubTaskProgress(progressId, progressPercentage);
    }

    @Override
    public List<RoutineTask> getAllRoutineTasksByUserId(int userID) {
          List<RoutineTask> routineTasks =   routineTaskRepository.getAllRoutineTasksByUserId(userID);
          for (RoutineTask routineTask : routineTasks){
              routineTask.setRoutineSubTasks(getRoutineSubTasksByParentId(routineTask.getId()));
          }
            return routineTasks;
    }

    @Override
    public Progress getProgressByTaskId(int taskID) {
        return routineTaskRepository.getProgressByTaskId(taskID);
    }

    @Override
    public List<RoutineSubTask> getRoutineSubTasksByParentId(int taskID) {
        List<RoutineSubTask> routineSubTasks = routineTaskRepository.getRoutineSubTasksByParentId(taskID);
        for (RoutineSubTask routineSubTask : routineSubTasks) {
            routineSubTask.setProgress(getProgressByTaskID(routineSubTask.getId()));
        }
        return  routineSubTasks;
    }

    @Override
    public Progress getProgressByTaskID(int taskID) {

            Progress tempProgress = routineTaskRepository.getProgressByTaskId(taskID);
            if (tempProgress != null) {
                System.out.println("SUCCESS : Get Old Progress" + tempProgress.toString());
                return tempProgress;
            }else {
                boolean status = false;
                try {
                    status = insertProgresForNewDay(taskID);
                } catch (Exception e) {
                    System.out.println("Add Progress more than one record : " + e.getLocalizedMessage());
                    status = false;
                }
                if (status) {
                    tempProgress = routineTaskRepository.getProgressByTaskId(taskID);
                    System.out.println("ERORR :Create New Progress DATA" + tempProgress.toString());
                    return tempProgress;
                } else {
                    System.out.println("ERORR :Create New Progress NULL" + "NULL");
                    return routineTaskRepository.getProgressByTaskId(taskID);
                }
            }

    }
}