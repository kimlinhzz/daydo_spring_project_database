package com.kshrd.daydo.service;

import com.kshrd.daydo.model.Motivation;
import com.kshrd.daydo.repository.MotivationRepo.MotivationRepository;
import com.kshrd.daydo.service.MotivationService.MotivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MotivationServiceImp implements MotivationService {

    @Autowired
    MotivationRepository motivationRepository;

    @Override
    public List<Motivation> getAllMotivations() {
        return motivationRepository.getAllMotivations();
    }

    @Override
    public boolean insertMotivation(Motivation motivation) {
        return motivationRepository.insertMotivation(motivation);
    }
}
