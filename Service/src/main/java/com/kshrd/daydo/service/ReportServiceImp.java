package com.kshrd.daydo.service;

import com.kshrd.daydo.model.Report;
import com.kshrd.daydo.model.ReportModel.DailyReport;
import com.kshrd.daydo.model.ReportModel.RoutineReport;
import com.kshrd.daydo.model.ReportModel.SubRoutineReport;
import com.kshrd.daydo.model.RoutineSubTask;
import com.kshrd.daydo.model.RoutineTask;
import com.kshrd.daydo.repository.ReportRepo.ReportRepository;
import com.kshrd.daydo.service.ReportService.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReportServiceImp implements ReportService {

    @Autowired
    ReportRepository reportRepository;

    @Override
    public List<DailyReport> getDailyReportOnDate(int userId, Timestamp fromDate, Timestamp toDate){
                //"2020-01-01 00:00:00.00"
        List<DailyReport> dailyReportList = reportRepository.getDailyReportOnDate(userId, fromDate, toDate);
        for (DailyReport dailyReport : dailyReportList) {
            List<RoutineReport> routineReportList = reportRepository.getAllRoutineReport(userId);
            Timestamp onDate = dailyReport.getProgressDate();
            for (RoutineReport routineReport : routineReportList) {
                List<SubRoutineReport> subRoutineReportList = routineReport.getSubRoutineReportList();
                routineReport.setTotalSubTaskProgressPercentage(reportRepository.getTotalProgressOnEachTask(routineReport.getTaskId(), onDate));
                for (SubRoutineReport subRoutineReport : subRoutineReportList) {
                    int subId = subRoutineReport.getSubTaskId();
                    subRoutineReport.setProgressReport(reportRepository.getProgressReportOnDate(subId, onDate));
                }
            }
            dailyReport.setRoutineTaskList(routineReportList);
        }
        int i = 0;
        LocalDateTime ldt = fromDate.toLocalDateTime();
        LocalDateTime tsToDate = toDate.toLocalDateTime();
        // Format

        List<LocalDateTime> localDateTimeList = new ArrayList<>();
        do{
            boolean status = true;
            for (DailyReport dailyReport : dailyReportList) {
                if ( getEqual(ldt, dailyReport.getProgressDate().toLocalDateTime()) ){
                    status = false;
                }
            }
            if (status == true) {
                List<RoutineReport> rt = new ArrayList<>();
                dailyReportList.add(i, new DailyReport(Timestamp.valueOf(ldt), 0, 0, 0.0, rt));
            }
            ldt = ldt.plusDays(1);
            localDateTimeList.add(ldt);
            i++;
         }while(!getEqual(ldt, tsToDate));

        return dailyReportList;
    }

    boolean getEqual(LocalDateTime ldt1, LocalDateTime ldt2){
         return (ldt1.getYear() == ldt2.getYear() && ldt1.getMonthValue() == ldt2.getMonthValue() && ldt1.getDayOfYear() == ldt2.getDayOfYear());
    }
}
