package com.kshrd.daydo.service;

import com.kshrd.daydo.model.SimplySubTask;
import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.repository.SmiplyTaskRepo.SimplyTaskRepository;
import com.kshrd.daydo.service.SimplyTaskService.SimplyTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimplyTaskServiceImp implements SimplyTaskService {

    @Autowired
    SimplyTaskRepository simplyTaskRepository;

    @Override
    public boolean insertSimplyTask(SimplyTask simplyTask) {
        return simplyTaskRepository.insertSimplyTask(simplyTask);
    }

    @Override
    public boolean insertSimplySubTask(SimplySubTask simplySubTask) {
        return simplyTaskRepository.insertSimplySubTask(simplySubTask);
    }

    @Override
    public boolean deleteSimplyTaskById(int taskID) {
        return simplyTaskRepository.deleteSimplyTaskById(taskID);
    }

    @Override
    public boolean deleteSimplySubTaskById(int taskSubID) {
        return simplyTaskRepository.deleteSimplySubTaskById(taskSubID);
    }

    @Override
    public boolean updateSimplyTasks(SimplyTask simplyTask) {
        return simplyTaskRepository.updateSimplyTasks(simplyTask);
    }

    @Override
    public boolean updateSimplySubTasks(SimplySubTask simplySubTask) {
        return simplyTaskRepository.updateSimplySubTasks(simplySubTask);
    }

    @Override
    public boolean updateToArchive(int taskID, boolean isArchive) {
        return simplyTaskRepository.updateToArchive(taskID , isArchive);
    }

    @Override
    public boolean deleteArchiveById(int taskID) {
        return simplyTaskRepository.deleteArchiveById(taskID);
    }

    @Override
    public List<SimplyTask> getAllArchiveByUserId(int userID) {
        return simplyTaskRepository.getAllArchiveByUserId(userID);
    }

    @Override
    public List<SimplyTask> getSimplyTasksByUserIdAndDate(int userID, String type) {
        return simplyTaskRepository.getSimplyTasksByUserIDAndDate(userID , type);
    }

    @Override
    public SimplyTask getSimplyTaskById(int taskID) {
        return simplyTaskRepository.getSimplyTaskById(taskID);
    }

    @Override
    public SimplyTask getLastestSimplyTasksByUserId(int userID) {
        return simplyTaskRepository.getLastestSimplyTasksByUserId(userID);
    }

    @Override
    public SimplySubTask getLastestSimplySubTasksByUserId( int taskID,int userID) {
        return simplyTaskRepository.getLastestSimplySubTasksByUserId(taskID,userID);
    }
}
