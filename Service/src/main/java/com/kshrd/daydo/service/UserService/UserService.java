package com.kshrd.daydo.service.UserService;

import com.kshrd.daydo.model.SimplySubTask;
import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService  {

    boolean addUser(User user);
    boolean updateUser(User user);
    boolean checkOldUser(String accessToken);
    List<SimplyTask> getCompleteSimplyTask(int userID);
    User loginUser(String accessToken);
    User findUserBySenderId(String senderID);
    User getUserById(int userID);
    List<User> getAllUser();
}
