package com.kshrd.daydo.service;

import com.kshrd.daydo.model.Notitification;
import com.kshrd.daydo.repository.NotificationRepo.NotificationRepository;
import com.kshrd.daydo.service.NotificationService.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImp implements NotificationService {

    NotificationRepository notificationRepository;

    @Override
    public boolean insertNotification(Notitification notification) {
        return notificationRepository.insertNotification(notification);
    }

    @Override
    public List<Notitification> getAllNotifications() {
        return notificationRepository.getAllNotifications();
    }

    @Override
    public List<Notitification> getNotificationByUserId(Integer userId) {
        return notificationRepository.getNotificationByUserId(userId);
    }

    @Override
    public Notitification getNotificationById(Integer id) {
        return notificationRepository.getNotificationById(id);
    }

    @Override
    public boolean updateIsRead(Integer id) {
        return notificationRepository.updateIsRead(id);
    }

    @Override
    public boolean updateIsDeleted(Integer id) {
        return notificationRepository.updateIsDeleted(id);
    }

    @Autowired
    public NotificationServiceImp(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }
}
