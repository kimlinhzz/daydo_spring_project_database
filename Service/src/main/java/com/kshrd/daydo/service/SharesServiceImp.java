package com.kshrd.daydo.service;

import com.kshrd.daydo.model.SharedTask;
import com.kshrd.daydo.repository.ShareTaskRepo.SharesRepository;
import com.kshrd.daydo.service.ShareTaskService.SharesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SharesServiceImp implements SharesService {
    @Autowired
    SharesRepository sharesRepository;

    @Override
    public boolean updateSimplyTaskIsShared(int taskId) {
        return sharesRepository.updateSimplyTaskIsShared(taskId);
    }

    @Override
    public boolean updateSimplySubTaskIsShared(int taskId) {
        return sharesRepository.updateSimplySubTaskIsShared(taskId);
    }

    @Override
    public boolean insertShareTask(SharedTask sharedTask) {
        return sharesRepository.insertShareTask(sharedTask);
    }

    @Override
    public Integer existedShare(SharedTask sharedTask){ return sharesRepository.existedShare(sharedTask);}

    @Override
    public List<SharedTask> getAllSharedTaskByUserId(int userId) {
        return  sharesRepository.getAllSharedTaskByUserId(userId);
    }

    @Override
    public boolean updateShareRoleByUserIdAndTaskId(int userId, int taskId, int roleId) {
        return  sharesRepository.updateShareRoleByUserIdAndTaskId(userId, taskId, roleId);
    }

    @Override
    public boolean deleteShareByShareId(int userId, int taskId) {
        return sharesRepository.deleteShareByShareId(userId, taskId);
    }

    @Override
    public boolean deleteShareTask(int userId, int taskId) {
        if (sharesRepository.isOwner(userId, taskId) == 1){
            // is task owner
            return  sharesRepository.deleteShareByOwner(userId, taskId);
        }
        else{
            return sharesRepository.deleteShareByUserIdTaskId(userId, taskId);
        }
    }

    @Override
    public boolean insertTaskToExistedUser(SharedTask sharedTask) {
        return  sharesRepository.insertTaskToExistedUser(sharedTask);
    }




}
