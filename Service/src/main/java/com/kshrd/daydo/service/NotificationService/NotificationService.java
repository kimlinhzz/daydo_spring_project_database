package com.kshrd.daydo.service.NotificationService;

import com.kshrd.daydo.model.Notitification;

import java.util.List;

public interface NotificationService {

    boolean insertNotification(Notitification notification);

    List<Notitification> getAllNotifications();

    List<Notitification> getNotificationByUserId(Integer userId);

    Notitification getNotificationById(Integer id);

    boolean updateIsRead(Integer id);

    boolean updateIsDeleted(Integer id);

}
