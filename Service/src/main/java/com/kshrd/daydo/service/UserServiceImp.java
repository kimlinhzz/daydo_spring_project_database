package com.kshrd.daydo.service;

import com.kshrd.daydo.model.SimplySubTask;
import com.kshrd.daydo.model.SimplyTask;
import com.kshrd.daydo.model.User;
import com.kshrd.daydo.repository.UserRepo.UserRepository;
import com.kshrd.daydo.service.UserService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public boolean addUser(User user) {
        return userRepository.addUser(user);
    }

    @Override
    public boolean updateUser(User user) {
        return userRepository.updateUser(user);
    }

    @Override
    public boolean checkOldUser(String accessToken) {
        return userRepository.checkUser(accessToken);
    }

    @Override
    public List<SimplyTask> getCompleteSimplyTask(int userID) {
        return userRepository.getCompleteSimplyTask(userID);
    }

    @Override
    public User loginUser(String accessToken) {
        return userRepository.loginUser(accessToken);
    }

    @Override
    public User findUserBySenderId(String senderID) {
        return userRepository.findUserBySenderId(senderID);
    }

    @Override
    public User getUserById(int userID) {
        return userRepository.getUserById(userID);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.getAllUsers();
    }


}
