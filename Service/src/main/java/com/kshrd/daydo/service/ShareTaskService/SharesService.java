package com.kshrd.daydo.service.ShareTaskService;

import com.kshrd.daydo.model.SharedTask;

import java.util.List;

public interface SharesService {
    boolean updateSimplyTaskIsShared(int taskId);
    boolean updateSimplySubTaskIsShared(int taskId);
    boolean insertShareTask(SharedTask sharedTask);
    Integer existedShare(SharedTask sharedTask);
    List<SharedTask> getAllSharedTaskByUserId(int userId);
    boolean updateShareRoleByUserIdAndTaskId(int userId, int taskId, int roleId);
    boolean deleteShareByShareId(int userId, int taskId);
    boolean deleteShareTask(int userId, int taskId);
    boolean insertTaskToExistedUser(SharedTask sharedTask);
}
